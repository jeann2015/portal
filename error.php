<?php

include ("db.php");

?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Clave Incorrecta</title>
	<link rel="stylesheet" href="css/bootstrap.min.css" media="screen" />
</head>
<body>
<table class="table">
	<tr>
		<td>
			<div align="center">
				Este usuario no Existe!
			</div>
		</td>
	</tr>
	<tr>
		<td>
			<div align="center">
			<a class="btn btn-primary" href="index.php">Volver al Login</a>
			</div>
		</td>
	</tr>
	
</table>
<script src="js/jquery.js"></script>
<script src="js/bootstrap.min.js"></script>
</body>
</html>
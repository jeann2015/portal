<?php


/**
 * 
 * @todo Funcion buscar usuarios de advisor
 * @author Jean Carlos Nuñez
 * @param string $var_usuario
 * @param string $var_clave
 * @return array
 *  
 */ 
function buscar_usuario_advisor($var_usuario,$var_clave)
{
	global $conn;
	$con=0;

	$sSql="select * from usuarios where usuario = '$var_usuario' and clave = '$var_clave'";
	


	$rs=phpmkr_query($sSql,$conn) or die("Fallo al ejecutar la consulta en la linea" . __LINE__ . ": " . phpmkr_error($conn) . '<br>SQL: ' . $sSql);
	while ($row_rs = $rs->fetch_assoc())
	{$var_codigo=$row_rs['codigo'];}

	$con = phpmkr_num_rows($rs);

	if($con>0){
		$retorno=array('0'=>$var_codigo);
	}
	else{
		$retorno=array('0'=>0);	
	}
		
	return $retorno;
};




/**
 * 
 * @todo Funcion para avisar que esta vivo el servicio para aplicacion advisor
 * @author Jean Carlos Nuñez
 * @param string $ip
 * @param string $server
 * @return array
 *  
 */ 
function avisar_advisor($ip,$server,$var_fecha,$var_hora)
{
	global $conn;
	
	$sSql="select estado from codigos";
	$rs=phpmkr_query($sSql,$conn) or die("Fallo al ejecutar la consulta en la linea" . __LINE__ . ": " . phpmkr_error($conn) . '<br>SQL: ' . $sSql);
	while ($row_rs = $rs->fetch_assoc())
	{$var_codigo=$row_rs['estado']+1;}
	$sSql="update codigos set estado = $var_codigo";
	$rs=phpmkr_query($sSql,$conn) or die("Fallo al ejecutar la consulta en la linea" . __LINE__ . ": " . phpmkr_error($conn) . '<br>SQL: ' . $sSql);
	
	$sSql="insert into estados values ($var_codigo,'$var_fecha','$var_hora',$server,'$ip')";
	$rs=phpmkr_query($sSql,$conn) or die("Fallo al ejecutar la consulta en la linea" . __LINE__ . ": " . phpmkr_error($conn) . '<br>SQL: ' . $sSql);
	
	$retorno=1;
		
	return $retorno;
};


/**
 * 
 * @todo Funcion que Asigna Mecanico a la salida temporal
 * @author Jean Carlos Nuñez
 * @param int $var_codigo_mecanico
 * @param int $var_cod_sal
 * @return array
 *  
 */ 
function asignar_mecanicos_jq($var_codigo_mecanico,$var_cod_sal)
{
	global $conn;
	
	
	$sSql="update salidas_temp set cedula_responsable = '$var_codigo_mecanico' where codigo = $var_cod_sal";
	errores($sSql);
	$rs=phpmkr_query($sSql,$conn) or die("Fallo al ejecutar la consulta en la linea" . __LINE__ . ": " . phpmkr_error($conn) . '<br>SQL: ' . $sSql);
	$con = phpmkr_num_rows($rs);
	
	$retorno=array('0'=>1);
	return $retorno;
};

/**
 * 
 * @todo Funcion que buscar facturado por empresa
 * @author Jean Carlos Nuñez
 * @param date $fecha
 * @return array
 *  
 */ 
function buscar_facturacion_empresas_jq($fecha)
{
	global $conn;
	
	
	$sSql="select sum(t.monto_dia) as monto,e.descripcion from tickets t,empresas e where t.fecha_impresion = '".fecha_sql($fecha)."' 
 	and tipo_pago = 1 and e.codigo = t.empresa and t.empresa in (select codigo from empresas) group by t.empresa desc";
	$rs=phpmkr_query($sSql,$conn) or die("Fallo al ejecutar la consulta en la linea" . __LINE__ . ": " . phpmkr_error($conn) . '<br>SQL: ' . $sSql);
	$con = phpmkr_num_rows($rs);
	$i=0;
	if($con>0)
	{	
		while ($row_rs = $rs->fetch_assoc())
		{

			$retorno[$i] = $row_rs;
			$i=$i+1;
		}
	}
	else{
		$retorno=array('0'=>0);
	}	
	return $retorno;
};

/**
 * 
 * @todo Funcion que buscar facturado general
 * @author Jean Carlos Nuñez
 * @param date $fecha
 * @return array
 *  
 */ 
function buscar_facturado_jq($fecha)
{
	global $conn;
	
	
	$sSql="select sum(t.monto_dia) as monto from tickets t where t.fecha_impresion = '".fecha_sql($fecha)."' and tipo_pago = 1";

	$rs=phpmkr_query($sSql,$conn) or die("Fallo al ejecutar la consulta en la linea" . __LINE__ . ": " . phpmkr_error($conn) . '<br>SQL: ' . $sSql);
	$con = phpmkr_num_rows($rs);
	
	if($con>0)
	{
		while ($row_rs = $rs->fetch_assoc())
		{$var_monto=number_format($row_rs['monto'],2);}
		
		$row_rs=array('0'=>$var_monto);
	}
	else{
		$row_rs=array('0'=>0);
	}	
	return $row_rs;
};

/**
* Resize an image and keep the proportions
* @author Jean Carlos Nuñez
* @param string $filename
* @param integer $max_width
* @param integer $max_height
* @return image
*/

function resizeImage($filename, $max_width, $max_height)
{
    list($orig_width, $orig_height) = getimagesize($filename);

    $width = $orig_width;
    $height = $orig_height;

    # taller
    if ($height > $max_height) {
        $width = ($max_height / $height) * $width;
        $height = $max_height;
    }

    # wider
    if ($width > $max_width) {
        $height = ($max_width / $width) * $height;
        $width = $max_width;
    }

    $image_p = imagecreatetruecolor($width, $height);

    $image = imagecreatefromjpeg($filename);

    imagecopyresampled($image_p, $image, 0, 0, 0, 0,
                                     $width, $height, $orig_width, $orig_height);

    return $image_p;
}

/**
 * 
 * @todo Funcion que escribe foto para ser mostrara
 * @author Jean Carlos Nuñez
 * @param int $num_ope
 * @return string
 *  
 */ 
function escribir_foto($num_ope,$conn)
{

	$path="../foto/";
	$fileExtension=array("image/jpeg"=>".jpg");
	
	$sSql="select foto from operadores_fotos where num_ope = $num_ope";
	$rs=phpmkr_query($sSql,$conn) 
	or die("Fallo al ejecutar la consulta en la linea" . __LINE__ . ": " . phpmkr_error($conn) . '<br>SQL: ' . $sSql);
	while ($row_rs = $rs->fetch_assoc()) 
	{  			
		$var_logo=$row_rs['foto'];			
	}	
			
	$var_nombre_foto = make_seed();	
	file_put_contents($path.$var_nombre_foto.$fileExtension['image/jpeg'],$var_logo, FILE_APPEND);

	
	return $var_nombre_foto;
}


/**
 * 
 * @todo Funcion que buscar numero de unidad para facturar ipago
 * @author Jean Carlos Nuñez
 * @param int $cedula
 * @return array
 *  
 */ 
function buscar_operador_cedula_jq($cedula)
{
	global $conn;
	
	
	$sSql="select a.num_und from autos a,operadores o where o.cedula = '$cedula' and a.num_und = o.num_und_asig";
	$rs=phpmkr_query($sSql,$conn) or die("Fallo al ejecutar la consulta en la linea" . __LINE__ . ": " . phpmkr_error($conn) . '<br>SQL: ' . $sSql);
	$con = phpmkr_num_rows($rs);
	if($con>0)
	{
		$row_rs = phpmkr_fetch_row($rs);
		$var_resultado[]=$row_rs;
	}else{$row_rs=array('0'=>0);}	
	return $row_rs;
};


/**
 * 
 * @todo Funcion que genera una funcion en javascript para listar los autos
 * @author Jean Carlos Nuñez
 * @return string
 *  
 */
function buscar_numero_ope_des()
{
	global $conn; 
	$rs = mysqli_query($conn,"select * from autos order by num_und");

	echo "<script>\n";
	echo "function buscar_ope_des()\n";
	echo "{\n";
	echo "var doc = document.form1; \n"; 
	echo "var var_num_und = doc.num_und_destino.value; \n";while ($row_rs = mysqli_fetch_array($rs)){$var_num_und=$row_rs['num_und'];$var_cod_emp=$row_rs['cod_emp'];$var_num_ope=$row_rs['num_ope'];
	echo "	if(var_num_und=='".$var_num_und."')\n";
	echo "	{ \n";
	echo "		doc.cod_emp_des.value=".$var_cod_emp."; \n";
	echo "	}\n"; }
	echo" 
	}
	</script>	
	\n";
		
};


/**
 * 
 * @todo Funcion que insertar la deuda de cxc normal a siniestro y cd cxc secundaria a cxc normal
 * @author Jean Carlos Nuñez
 * @param string $num_und
 * @param string $num_und_des
 * @param int $var_cod_usu
 * @return array
 *  
 */
  
function deuda_operador_cxc_sec_cxc($num_und,$num_und_des,$var_cod_usu)
{
	global $conn;
		
	$rs=phpmkr_query("select cod_emp,num_ope from autos where num_und = '$num_und'",$conn) 
	or die("Fallo al ejecutar la consulta en la linea" . __LINE__ . ": " . phpmkr_error($conn) . '<br>SQL: ' . $sSql);
	while ($row_rs = $rs->fetch_assoc())
	{$var_num_ope=$row_rs['num_ope'];}
	
	$rs=phpmkr_query("select cedula from operadores where num_oper = $var_num_ope",$conn) 
	or die("Fallo al ejecutar la consulta en la linea" . __LINE__ . ": " . phpmkr_error($conn) . '<br>SQL: ' . $sSql);
	while ($row_rs = $rs->fetch_assoc())
	{$var_cedula=$row_rs['cedula'];}
	
	$var_monto=0;
	$rs=phpmkr_query("select sum(monto) as monto from cuentasxcobrar where num_ope = $var_num_ope",$conn) 
	or die("Fallo al ejecutar la consulta en la linea" . __LINE__ . ": " . phpmkr_error($conn) . '<br>SQL: ' . $sSql);
	while ($row_rs = $rs->fetch_assoc())
	{$var_monto=$row_rs['monto'];}
	
	
	//Siniestro
	if($var_monto>0){
		$var_fecha_actual = fecha_aplicacion_mysql($conn);
		$rs=phpmkr_query("select siniestros as codigo from codigos",$conn) 
		or die("Fallo al ejecutar la consulta en la l?nea" . __LINE__ . ": " . phpmkr_error($conn) . '<br>SQL: ' . $sSql);
		while ($row_rs = $rs->fetch_assoc())
		{$var_codigo=$row_rs['codigo']+1;}	
		if ($var_codigo==0){$var_codigo=1;}
		
		phpmkr_query("update codigos set siniestros = $var_codigo",$conn) 
		or die("Fallo al ejecutar la consulta en la linea" . __LINE__ . ": " . phpmkr_error($conn) . '<br>SQL: ' . $sSql);	
		
		$rs=phpmkr_query("select sum(monto) as monto from siniestros where num_ope = $var_num_ope",$conn) 
		or die("Fallo al ejecutar la consulta en la linea" . __LINE__ . ": " . phpmkr_error($conn) . '<br>SQL: ' . $sSql);
		while ($row_rs = $rs->fetch_assoc())
		{$var_monto_siniestros=$row_rs['monto'];}	
		if($var_monto_siniestros==""){$var_monto_siniestros=$var_monto;}	
		
		$sSql="insert into siniestros values($var_codigo,$var_num_ope,'$var_fecha_actual',$var_monto,$var_monto,$var_monto_siniestros,
		1,'$var_cedula',$var_cod_usu,'$num_und',1,'2','DIARIOS PASADO A SINIESTRO POR PRESTAMO DE AUTO EN DIFERENTE EMPRESA','','','','Culpable','',0,0)";
		$rs=phpmkr_query($sSql,$conn) or die("Fallo al ejecutar la consulta en la linea" . __LINE__ . ": " . phpmkr_error($conn) . '<br>SQL: ' . $sSql);
		//Siniestro
		
		$sSql="delete from cuentasxcobrar where num_ope = $var_num_ope";
		$rs=phpmkr_query($sSql,$conn) 
		or die("Fallo al ejecutar la consulta en la linea" . __LINE__ . ": " . phpmkr_error($conn) . '<br>SQL: ' . $sSql);
		auditoria($var_cod_usu,"SE TRASLADO LA CUENTA POR COBRAR DEL OPERADOR: ".$var_num_ope." YA QUE SE CAMBIO A UNA UNIDAD: ".$num_und_des." DE OTRA EMPRESA A SINIESTROS",$conn);	
		
	}

	$sSql="insert into cuentasxcobrar (num_ope,fecha,monto)
	SELECT num_ope,fecha,monto
	FROM cuentasxcobrar_secundaria where num_ope = $var_num_ope";
	$rs=phpmkr_query($sSql,$conn) 
	or die("Fallo al ejecutar la consulta en la linea" . __LINE__ . ": " . phpmkr_error($conn) . '<br>SQL: ' . $sSql);
	
	$sSql="delete from cuentasxcobrar_secundaria where num_ope = $var_num_ope";
	$rs=phpmkr_query($sSql,$conn) 
	or die("Fallo al ejecutar la consulta en la linea" . __LINE__ . ": " . phpmkr_error($conn) . '<br>SQL: ' . $sSql);
	
	auditoria($var_cod_usu,"SE TRASLADO LA CUENTA POR COBRAR DEL OPERADOR: ".$var_num_ope." DE CUENTA POR COBRAR BACKUP A CUENTA POR COBRAR NORMAL",$conn);	
	
	return 1;
}


/**
 * 
 * @todo Funcion que busca cxc secundaria del operador
 * @author Jean Carlos Nuñez
 * @param int $num_ope
 * @return array
 *  
 */
  
function buscar_num_ope_cxc_secundaria_jq($num_ope)
{
	global $conn;
	$con=0;
	$fecha = fecha_sql($fecha);
	$sSql="select * from cuentasxcobrar_secundaria where num_ope = $num_ope";
	$rs=phpmkr_query($sSql,$conn) or die("Fallo al ejecutar la consulta en la linea" . __LINE__ . ": " . phpmkr_error($conn) . '<br>SQL: ' . $sSql);
	$con = phpmkr_num_rows($rs); 
	$row_rs = $rs->fetch_assoc();
	$row_rs=array('0'=>$con,'1'=>$row_rs['num_und']);
	return $row_rs;
}

/**
 * 
 * @todo Funcion que insertar la deuda en siniestro desde cxc si son empresas diferentes
 * @author Jean Carlos Nuñez
 * @param string $num_und
 * @param string $num_und_des
 * @param int $var_cod_usu
 * @return array
 *  
 */
  
function deuda_operador_cxc_sini($num_und,$num_und_des,$var_cod_usu)
{
	global $conn;
		
	$rs=phpmkr_query("select cod_emp,num_ope from autos where num_und = '$num_und'",$conn) 
	or die("Fallo al ejecutar la consulta en la linea" . __LINE__ . ": " . phpmkr_error($conn) . '<br>SQL: ' . $sSql);
	while ($row_rs = $rs->fetch_assoc())
	{$var_num_ope=$row_rs['num_ope'];}
	
	$rs=phpmkr_query("select cedula from operadores where num_oper = $var_num_ope",$conn) 
	or die("Fallo al ejecutar la consulta en la linea" . __LINE__ . ": " . phpmkr_error($conn) . '<br>SQL: ' . $sSql);
	while ($row_rs = $rs->fetch_assoc())
	{$var_cedula=$row_rs['cedula'];}
	
	$var_monto=0;
	$rs=phpmkr_query("select sum(monto) as monto from cuentasxcobrar where num_ope = $var_num_ope",$conn) 
	or die("Fallo al ejecutar la consulta en la linea" . __LINE__ . ": " . phpmkr_error($conn) . '<br>SQL: ' . $sSql);
	while ($row_rs = $rs->fetch_assoc())
	{$var_monto=$row_rs['monto'];}
	
	
	//Siniestro
	if($var_monto>0){
		$var_fecha_actual = fecha_aplicacion_mysql($conn);
		$rs=phpmkr_query("select siniestros as codigo from codigos",$conn) 
		or die("Fallo al ejecutar la consulta en la l?nea" . __LINE__ . ": " . phpmkr_error($conn) . '<br>SQL: ' . $sSql);
		while ($row_rs = $rs->fetch_assoc())
		{$var_codigo=$row_rs['codigo']+1;}	
		if ($var_codigo==0){$var_codigo=1;}
		
		phpmkr_query("update codigos set siniestros = $var_codigo",$conn) 
		or die("Fallo al ejecutar la consulta en la linea" . __LINE__ . ": " . phpmkr_error($conn) . '<br>SQL: ' . $sSql);	
		
		$rs=phpmkr_query("select sum(monto) as monto from siniestros where num_ope = $var_num_ope",$conn) 
		or die("Fallo al ejecutar la consulta en la linea" . __LINE__ . ": " . phpmkr_error($conn) . '<br>SQL: ' . $sSql);
		while ($row_rs = $rs->fetch_assoc())
		{$var_monto_siniestros=$row_rs['monto'];}	
		if($var_monto_siniestros==""){$var_monto_siniestros=$var_monto;}	
		
		$sSql="insert into siniestros values($var_codigo,$var_num_ope,'$var_fecha_actual',$var_monto,$var_monto,$var_monto_siniestros,
		1,'$var_cedula',$var_cod_usu,'$num_und',1,'2','DIARIOS PASADO A SINIESTRO POR PRESTAMO DE AUTO EN DIFERENTE EMPRESA','','','','Culpable','',0,0)";
		$rs=phpmkr_query($sSql,$conn) or die("Fallo al ejecutar la consulta en la linea" . __LINE__ . ": " . phpmkr_error($conn) . '<br>SQL: ' . $sSql);
		//Siniestro
		
		$sSql="delete from cuentasxcobrar where num_ope = $var_num_ope";
		$rs=phpmkr_query($sSql,$conn) 
		or die("Fallo al ejecutar la consulta en la linea" . __LINE__ . ": " . phpmkr_error($conn) . '<br>SQL: ' . $sSql);
		auditoria($var_cod_usu,"SE TRASLADO LA CUENTA POR COBRAR DEL OPERADOR: ".$var_num_ope." YA QUE SE CAMBIO A UNA UNIDAD DE OTRA EMPRESA: ".$num_und_des." A SINIESTROS",$conn);	
		
	}

	return 1;
}

/**
 * 
 * @todo Funcion que insertar la deuda del operador en la tabla de cuentasxcobrar_secundaria
 * @author Jean Carlos Nuñez
 * @param string $num_und
 * @param string $num_und_des
 * @param int $var_cod_usu
 * @return array
 *  
 */
  
function deuda_operador_cxc_sec($num_und,$num_und_des,$var_cod_usu)
{
	global $conn;
	
	$rs=phpmkr_query("select cod_emp,num_ope from autos where num_und = '$num_und_des'",$conn) 
	or die("Fallo al ejecutar la consulta en la linea" . __LINE__ . ": " . phpmkr_error($conn) . '<br>SQL: ' . $sSql);
	while ($row_rs = $rs->fetch_assoc())
	{$var_empresa_des=$row_rs['cod_emp'];$var_num_ope=$row_rs['num_ope'];}
	
	$rs=phpmkr_query("select cod_emp,num_ope from autos where num_und = '$num_und'",$conn) 
	or die("Fallo al ejecutar la consulta en la linea" . __LINE__ . ": " . phpmkr_error($conn) . '<br>SQL: ' . $sSql);
	while ($row_rs = $rs->fetch_assoc())
	{$var_empresa_ori=$row_rs['cod_emp'];$var_num_ope=$row_rs['num_ope'];}
	
	if($var_empresa_des<>$var_empresa_ori)
	{
		$sSql="INSERT INTO cuentasxcobrar_secundaria (num_ope,fecha,monto,empresa,num_und)
		SELECT num_ope,fecha,monto,$var_empresa_ori,'$num_und'
		FROM cuentasxcobrar where num_ope = $var_num_ope";
		
		
	}
		   
	$rs=phpmkr_query($sSql,$conn) 
	or die("Fallo al ejecutar la consulta en la linea" . __LINE__ . ": " . phpmkr_error($conn) . '<br>SQL: ' . $sSql);
	
	$sSql="delete from cuentasxcobrar where num_ope = $var_num_ope";
	$rs=phpmkr_query($sSql,$conn) 
	or die("Fallo al ejecutar la consulta en la linea" . __LINE__ . ": " . phpmkr_error($conn) . '<br>SQL: ' . $sSql);
	
	auditoria($var_cod_usu,"SE TRASLADO LA CUENTA POR COBRAR DEL OPERADOR: ".$var_num_ope." YA QUE SE CAMBIO A UNA UNIDAD DE OTRA EMPRESA:".$num_und_des,$conn);	
	
	return 1;
}


/**
 * 
 * @todo Funcion que cambio de fecha de entrega consignatario
 * @author Jean Carlos Nuñez
 * @param int $codigo_temp
 * @param date $fecha
 * @return array
 *  
 */
  
function entrega_jq($codigo_temp,$fecha,$detalle)
{
	global $conn;
	$fecha = fecha_sql($fecha);
	$sSql="update bolsa_dinero set entrega_consignatario = '$fecha',detalle='$detalle' where numero_comprobante = '$codigo_temp'";
	errores($sSql);
	$rs=phpmkr_query($sSql,$conn) or die("Fallo al ejecutar la consulta en la linea" . __LINE__ . ": " . phpmkr_error($conn) . '<br>SQL: ' . $sSql);
	
	$row_rs=array('0'=>1);
	return $row_rs;
}


/**
 * 
 * @todo Funcion que eliminar sello de tabla de bolsa de dinero
 * @author Jean Carlos Nuñez
 * @param string $numero_comprobante
 * @param string $numero_sello
 * @return array
 *  
 */
  
function eliminar_numero_sello_n_jq($codigo_temp,$numero_sello)
{
	global $conn;
	$sSql="delete from bolsa_dinero  where codigo = $codigo_temp and numero_sello = '$numero_sello'";
	$rs=phpmkr_query($sSql,$conn) or die("Fallo al ejecutar la consulta en la linea" . __LINE__ . ": " . phpmkr_error($conn) . '<br>SQL: ' . $sSql);
	
	$row_rs=array('0'=>1);
	return $row_rs;
}
/**
 * 
 * @todo Funcion que eliminar sello e tabla de bolsa de dinero temporal
 * @author Jean Carlos Nuñez
 * @param string $numero_comprobante
 * @param string $numero_sello
 * @return array
 *  
 */
  
function eliminar_numero_sello_jq($codigo_temp,$numero_sello)
{
	global $conn;
	$sSql="delete from bolsa_dinero_temp  where codigo = $codigo_temp and numero_sello = '$numero_sello'";
	$rs=phpmkr_query($sSql,$conn) or die("Fallo al ejecutar la consulta en la linea" . __LINE__ . ": " . phpmkr_error($conn) . '<br>SQL: ' . $sSql);
	
	$row_rs=array('0'=>1);
	return $row_rs;
}

/**
 * 
 * @todo Funcion que busca numero de sello por comprobante
 * @author Jean Carlos Nuñez
 * @param string $numero_comprobante
 * @param string $numero_sello
 * @return array
 *  
 */
  
function buscar_numero_sello_jq($numero_comprobante,$numero_sello)
{
	global $conn;
	$sSql="select * from bolsa_dinero_temp  where numero_comprobante = '$numero_comprobante' and numero_sello = '$numero_sello'";
	$rs=phpmkr_query($sSql,$conn) or die("Fallo al ejecutar la consulta en la linea" . __LINE__ . ": " . phpmkr_error($conn) . '<br>SQL: ' . $sSql);
	$con = phpmkr_num_rows($rs);
	if($con>0)
	{
		$row_rs=array('0'=>1);
	}
	else
	{
		$row_rs=array('0'=>0);
	}
	return $row_rs;
}

/**
 * 
 * @todo Funcion que busca numero aleatorio apartir de una semilla
 * @author Jean Carlos Nuñez
 * @return int
 *  
 */

function make_seed()
{
  list($usec, $sec) = explode(' ', microtime());
  srand((float) $sec + ((float) $usec * 10));
  $randval = rand();
  return $randval; 
}

/**
 * 
 * @todo Funcion que busca operadores en la base de datos de yellowcar
 * @author Jean Carlos Nuñez
 * @param string $var_num_und
 * @param db $conn
 * @return array
 *  
 */
  
function buscar_unidad_operador_jq($var_num_und)
{
	global $conn;
	
	$sSql="select o.num_oper,a.num_und,o.cedula,o.celular1,concat(o.nombre,' ',o.apellido) as nombre from autos a
	left JOIN operadores o ON o.num_und_asig=a.num_und where a.num_und = '$var_num_und' limit 1";
		
	$rs=phpmkr_query($sSql,$conn) or die("Fallo al ejecutar la consulta en la linea" . __LINE__ . ": " . phpmkr_error($conn) . '<br>SQL: ' . $sSql);
	$con = phpmkr_num_rows($rs);
	if($con>0)
	{
		$row_rs = phpmkr_fetch_row($rs);
		$var_resultado[]=$row_rs;
	}else{$row_rs=array('0'=>999999);}
	return $row_rs;
}

/**
 * 
 * @todo Funcion que valida la ip autorizada
 * @author Jean Carlos Nuñez
 * @param string $ip
 * @return int
 *  
 */
function validar_ip_autorizada($var_ip)
{
	global $conn;
	
	$contador=0;
	$sSql="select count(*) as contador from ip_autorizadas where ip = '$var_ip'";
	
	$rs=phpmkr_query($sSql,$conn) 
	or die("Fallo al ejecutar la consulta en la linea" . __LINE__ . ": " . phpmkr_error($conn) . '<br>SQL: ' . $sSql);
	while ($row_rs = $rs->fetch_assoc())
	{$contador=$row_rs['contador'];}
	$contador=1;
	return $contador;
}

/**
 * 
 * @todo Funcion que corrige ortograficamente texto en español
 * @author Jean Carlos Nuñez
 * @param string $text
 * @param string $type
 * @param string $lenguague
 * @return string
 *  
 */
function spellChecker($text, $type = "html", $language = "spanish")
{
return stripslashes(file_get_contents("http://spellcheckerphp.com/api/?type=". $type ."&language=". $language ."&text=". urlencode($text)));
}


/**
 * 
 * @todo Funcion que trae a todos tecnico por codigo de sucursal
 * @author Jean Carlos Nuñez
 * @param int $var_cod_suc
 * @return array
 *  
 */ 
function tecnicos_jq($var_cod_suc)
{
	global $conn;
	
	$sSql="select codigo,nombre from tecnicos where cod_suc = $var_cod_suc";	
	$rs=phpmkr_query($sSql,$conn) or die("Fallo al ejecutar la consulta en la linea" . __LINE__ . ": " . phpmkr_error($conn) . '<br>SQL: ' . $sSql);
	$con = phpmkr_num_rows($rs);
	if($con>0)
	{
		while ($row_rs = $rs->fetch_assoc())
		{
			$var_resultado[] = $row_rs;
		}
		
		//print_r($var_resultado);
	}
	
	else{$var_resultado[]=array('codigo'=>0);}	
	return $var_resultado;
};

/**
 * 
 * @todo Funcion busca las empresas por usuario 
 * @author Jean Carlos Nuñez
 * @param int $var_cod_usu
 * @param db $conne
 * @return array
 *  
 */ 
function usuario_empresa($var_cod_usu,$conne)
{
	$sSql="select empresas from usuarios_empresas where cod_usu = ".$var_cod_usu."";	
	$rs=phpmkr_query($sSql,$conne) 
	or die("Fallo al ejecutar la consulta en la linea" . __LINE__ . ": " . phpmkr_error($conne) . '<br>SQL: ' . $sSql);	
	while ($row_rs = $rs->fetch_assoc())
	{
		$var_cod_emp[] = $row_rs['empresas'];
	}

	$implode_empresas_comas = implode(",", $var_cod_emp);
	if($implode_empresas_comas<>"")
	{
		return "(".$implode_empresas_comas.")";
	}
	else
	{
		return "(0)";
	}
}


/**
 * 
 * @todo Funcion buscar por numero de unidad el kilometraje de las unidades
 * @author Jean Carlos Nuñez
 * @param string $str
 * @return array
 *  
 */ 
function errores($str)
{
	error_log($str, 0);
}
/**
 * 
 * @todo Funcion que modifica la unidad y el kilometraje en la salida
 * @author Jean Carlos Nuñez
 * @param string $var_num_und
 * @return array
 *  
 */ 
function modificar_unidad_salida($var_num_und,$var_kilometraje,$var_salida)
{
	global $conn;
	
	$sSql="update salidas set kilometraje = $var_kilometraje,num_und = $var_num_und  where codigo = $var_salida";
	phpmkr_query($sSql,$conn) or die("Fallo al ejecutar la consulta en la linea" . __LINE__ . ": " . phpmkr_error($conn) . '<br>SQL: ' . $sSql);
	$row_rs=array('0'=>1);	
	return $row_rs;
};

/**
 * 
 * @todo Funcion cambia la ubicacion de las uninades
 * @author Jean Carlos Nuñez
 * @param int $var_cod_ubi
 * @param string $var_num_und
 * @return array
 *  
 */
 
function ubicacion_unidad_jq($var_cod_ubi,$var_num_und,$var_cod_usuario)
{
	global $conn; 
	$var_fecha = date("Y-m-d");
	$var_hora = date("H:i:s");
	
	$sSql="delete from ubicacion where num_und = $var_num_und";
	$rs=phpmkr_query($sSql,$conn) or die("Fallo al ejecutar la consulta en la linea" . __LINE__ . ": " . phpmkr_error($conn) . '<br>SQL: ' . $sSql);
	
	$sSql="insert into ubicacion values($var_num_und,$var_cod_ubi,'$var_fecha','$var_hora',$var_cod_usuario)";
	$rs=phpmkr_query($sSql,$conn) or die("Fallo al ejecutar la consulta en la linea" . __LINE__ . ": " . phpmkr_error($conn) . '<br>SQL: ' . $sSql);
	$row_rs=array('0'=>1);	
	return $row_rs;
}

/**
 * 
 * @todo Funcion que guarda que se entrego la cuenta x cobrar a operador
 * @author Jean Carlos Nuñez
 * @param string $var_num_salida
 * @param string $var_cod_usu
 * @return array
 *  
 */
 
function entregado_operador_jq($var_num_salida,$var_cod_usu)
{
	global $conn; 
	$var_fecha = date("Y-m-d");
	$var_hora = date("H:i:s");
	$sSql="insert into entregado_operador values($var_num_salida,'$var_fecha','$var_hora',$var_cod_usu)";
	$rs=phpmkr_query($sSql,$conn) or die("Fallo al ejecutar la consulta en la linea" . __LINE__ . ": " . phpmkr_error($conn) . '<br>SQL: ' . $sSql);
	$row_rs=array('0'=>1);	
	return $row_rs;
}


/**
 * 
 * @todo Funcion buscar por numero de unidad el kilometraje de las unidades
 * @author Jean Carlos Nuñez
 * @param string $var_num_und
 * @return array
 *  
 */ 
function buscar_auto_salidas_jq($var_num_und)
{
	global $conn;
	
	$sSql="select cobrar,kilometraje,modelo from autos where num_und = '$var_num_und'";
	$rs=phpmkr_query($sSql,$conn) or die("Fallo al ejecutar la consulta en la linea" . __LINE__ . ": " . phpmkr_error($conn) . '<br>SQL: ' . $sSql);
	$con = phpmkr_num_rows($rs);
	if($con>0)
	{
		$row_rs = phpmkr_fetch_row($rs);
		$var_resultado[]=$row_rs;
	}else{$row_rs=array('0'=>0);}	
	return $row_rs;
};


/**
 * 
 * @todo Funcion calcular cuanta memoria se usa por cada scritp invocadop
 * @author Jean Carlos Nuñez
 * @param string $archivo
 * @param string $memoria
 * @param dn $conne
 * @return Null
 *  
 */ 
function memoria($archivo,$memoria,$conne)
{
	$hora = date("H:i:s");
	$fecha = date("Y-m-d");
	$memoria=($memoria/1024)/1024;
	$sSql="insert into uso_memoria values('$archivo',$memoria,'$fecha','$hora');";
	phpmkr_query($sSql,$conne);
}
/**
 * 
 * @todo Funcion para calculo de lo facturado por los usuarios del sistema
 * @author Jean Carlos Nuñez
 * @param int $var_cod_usu
 * @param date $var_fecha
 * @return decimal
 *  
 */ 
function facturacion_usuario_monto_actual_todo($var_cod_usu_cajero,$var_fecha)
{

	global $conn; 	
	
	$sSql="select codigo from usuarios where estado = 1 and codigo = ".$var_cod_usu_cajero."";
	$rs_usu = mysqli_query($conn,$sSql);
	while ($row_rs_usu = mysqli_fetch_array($rs_usu))
	{		
		$var_cod_usu = $row_rs_usu['codigo'];
		$sSql="select sum(monto_dia) as monto_pagado
		from tickets where fecha_impresion between DATE_SUB('$var_fecha',INTERVAL 2 DAY)  and DATE_SUB('$var_fecha',INTERVAL 1 DAY) and cod_usu = ".$var_cod_usu." and tipo_pago = 1";
		//echo $sSql;
		$rs_tickets = mysqli_query($conn,$sSql);
		while ($rs_tickets_data = mysqli_fetch_array($rs_tickets))
		{$tickets=$rs_tickets_data['monto_pagado'];}
		if($tickets==""){$tickets=0;}

		$sSql="select sum(monto) as monto_pagado
		from siniestros_hist where fecha between DATE_SUB('$var_fecha',INTERVAL 2 DAY) and DATE_SUB('$var_fecha',INTERVAL 1 DAY) and cod_usu = ".$var_cod_usu." and tipo_pago = 1";
		//echo $sSql;
		$rs_siniestros = mysqli_query($conn,$sSql);
		while ($rs_siniestros_data = mysqli_fetch_array($rs_siniestros))
		{$siniestro=$rs_siniestros_data['monto_pagado'];}
		if($siniestro==0){$siniestro=0;}

		$sSql="select sum(monto) as monto_pagado
		from multas_hist where fecha between DATE_SUB('$var_fecha',INTERVAL 2 DAY) and DATE_SUB('$var_fecha',INTERVAL 1 DAY) and cod_usu = ".$var_cod_usu." and tipo_pago = 1";
		//echo $sSql;
		$rs_multas = mysqli_query($conn,$sSql);
		while ($rs_multas_data = mysqli_fetch_array($rs_multas))
		{$multas = $rs_multas_data['monto_pagado'];}
		if($multas==""){$multas=0;}

		$sSql="select sum(monto) as monto_pagado
		from inscripciones_hist where fecha between DATE_SUB('$var_fecha',INTERVAL 2 DAY) and DATE_SUB('$var_fecha',INTERVAL 1 DAY) and cod_usu = ".$var_cod_usu." and tipo_pago = 1";
		//echo $sSql;
		$rs_inscripciones = mysqli_query($conn,$sSql);
		while ($rs_inscripciones_data = mysqli_fetch_array($rs_inscripciones))
		{$inscripciones = $rs_inscripciones_data['monto_pagado'];}
		if($inscripciones==""){$inscripciones=0;}

		$sSql="select sum(monto) as monto_pagado
		from ahorro_mantenimiento_hist where fecha = between DATE_SUB('$var_fecha',INTERVAL 2 DAY) and DATE_SUB('$var_fecha',INTERVAL 1 DAY) and cod_usu = ".$var_cod_usu." and tipo_pago = 1";
		$rs_ahorro_man = mysqli_query($conn,$sSql);
		while ($rs_ahorro_man_data = mysqli_fetch_array($rs_ahorro_man))
		{$ahorro_man = $rs_ahorro_man_data['monto_pagado'];}
		if($ahorro_man==""){$ahorro_man=0;}
		
		$sSql="select sum(monto) as monto_pagado
		from ahorros_hist where fecha between DATE_SUB('$var_fecha',INTERVAL 2 DAY) and DATE_SUB('$var_fecha',INTERVAL 1 DAY) and cod_usu = ".$var_cod_usu." and tipo_pago = 1";
		//echo $sSql;
		$rs_ahorro = mysqli_query($conn,$sSql);
		while ($rs_ahorro_data = mysqli_fetch_array($rs_ahorro))
		{$ahorro = $rs_ahorro_data['monto_pagado'];}
		if($ahorro==""){$ahorro=0;}

		$sSql="select sum(monto) as monto_pagado
		from ahorro_dias_feriados_hist where fecha between DATE_SUB('$var_fecha',INTERVAL 2 DAY) and DATE_SUB('$var_fecha',INTERVAL 1 DAY) and cod_usu = ".$var_cod_usu." and tipo_pago = 1";
		//echo $sSql;
		$rs_ahorro_feriados = mysqli_query($conn,$sSql);
		while ($rs_ahorro_feriados_data = mysqli_fetch_array($rs_ahorro_feriados))
		{$ahorro_feri = $rs_ahorro_feriados_data['monto_pagado'];}
		if($ahorro_feri==""){$ahorro_feri=0;}

		$sSql="select sum(monto_facturado) as monto_pagado
	    from cuadre_caja where cod_usu_cajero = $var_cod_usu and fecha between DATE_SUB('$var_fecha',INTERVAL 2 DAY) and '$var_fecha'";
	    $rs_cuadre_caja = mysqli_query($conn,$sSql);
	    while ($rs_cuadre_caja_data = mysqli_fetch_array($rs_cuadre_caja))
	    {$diferencia = $rs_cuadre_caja_data['monto_pagado'];}   
		if($diferencia==""){$diferencia=0;}

		$sSql="select sum(monto_calculado) as monto_pagado
	    from cuadre_caja where cod_usu_cajero = $var_cod_usu and fecha between DATE_SUB('$var_fecha',INTERVAL 2 DAY) and '$var_fecha'";
	    //echo $sSql;
	    $rs_monto_calculado = mysqli_query($conn,$sSql);
	    while ($rs_monto_calculado_data = mysqli_fetch_array($rs_monto_calculado))
	    {$monto_calculado = $rs_monto_calculado_data['monto_pagado'];}
		    
		if($monto_calculado==""){$monto_calculado=0;}
	    
	    $sSql="select sum(diferencia) as monto_pagado
	    from cuadre_caja where fecha between DATE_SUB('$var_fecha',INTERVAL 2 DAY) and '$var_fecha' and cod_usu_cajero = ".$var_cod_usu."";
	    $rs_cuadre_caja = mysqli_query($conn,$sSql);
	    while ($rs_cuadre_caja_data = mysqli_fetch_array($rs_cuadre_caja))
	    {$diferencia_real = $rs_cuadre_caja_data['monto_pagado'];}    
		if($diferencia_real==""){$diferencia_real=0;}
	        
	    $diferencia = $diferencia_real + $diferencia + $monto_calculado;
	    //echo " -".$diferencia."- ";
	    $total_facturado = ($tickets+$siniestro+$multas+$inscripciones+$ahorro+$ahorro_feri+$ahorro_man)-($diferencia);
	    if($total_facturado<=0){$total_facturado=0.00;}
	}
	return $total_facturado;
};

/**
 * 
 * @todo Funcion para creacion de modal
 * @author Jean Carlos Nuñez
 * @param string $var_num_und
 * @param int $var_num_ope
 * @param int $var_empresa
 * @param date $var_fecha_desde
 * @param date $var_fecha_hasta
 * @return html
 *  
 */ 
function creacion_modal($var_num_und,$var_num_ope,$var_empresa,$var_fecha_desde,$var_fecha_hasta)
{
	$sSql="select fecha_impresion,monto_dia,kilometros from compuzul_nucleo_1.tickets 
	where num_auto ='".$var_num_und."' and 
	fecha_impresion between '".$var_fecha_desde."' and '".$var_fecha_hasta."' and num_ope = ".$var_num_ope."";
	$rs=phpmkr_query($sSql,$conn) 
	or die("Fallo al ejecutar la consulta en la linea" . __LINE__ . ": " . phpmkr_error($conn) . '<br>SQL: ' . $sSql);
	

		echo "
			<div id='renta<?php echo $var_num_und; ?>' class='modal hide fade' tabindex='' width='' role='dialog' aria-labelledby='myModalLabel' aria-hidden='true'>
			  <div class='modal-header'>
			  <button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button>
			  <h3 id='myModalLabel'>Detalle de Renta Diaria</h3>
			  </div>
			  <div class='modal-body'>
			  <table class='table table-hover' border='1'>
			    <tr>
			      <td>
			        Auto
			      </td>
			      <td title='Fecha de Impresion de Ticket'>
			        Fecha de Imp.
			      </td>
			      <td title='Fecha de Pago de Diario'>
			        Fecha Dia.
			      </td>
			      <td>
			        Monto Pagado
			      </td>
			      <td>
			        Kilometraje
			      </td>
			    </tr>";
			     
					while ($row_rs = $rs->fetch_assoc())
					{
						$var_fecha_impresion=$row_rs['fecha_impresion'];
						$var_monto_dia=$row_rs['monto_dia'];
						$var_kilometros=$row_rs['kilometros'];
			    
			    
			    echo "
			    <tr>
			      <td>
			      	$var_fecha_impresion;
			      </td>
			      <td>

			      </td>
			      <td>

			      </td>
			      <td>

			      </td>
			      <td>
			        
			      </td>
			    </tr>
			    ";
			}
			   echo "
			  </table>
			  </div>
			  <div class='modal-footer'>
			  <button class='btn' data-dismiss='modal'>Cerrar</button>
			  
			  </div>
			</div>
		";	
}

/**
 * 
 * @todo Funcion que registra en el secret los operadores
 * @author Jean Carlos Nuñez
 * @param db $conn
 * @param string $descripcion
 * @param string $cedula
 * @param string $nombre_operador
 * @param int $cod_empresa_sc
 * @return Null
 *  
 */ 
function operadores_secret($conn,$descripcion,$cedula,$nombre_operador,$cod_empresa_sc)
{
	$rs=phpmkr_query("select secret from codigos ",$conn) 
	or die("Fallo al ejecutar la consulta en la l?nea" . __LINE__ . ": " . phpmkr_error($conn) . '<br>SQL: ' . $sSql);
	while ($row_rs = $rs->fetch_assoc())
	{$var_secret=$row_rs['secret']+1;}

	$rs=phpmkr_query("select codigo from item_secret where descripcion='".$descripcion."'",$conn) 
	or die("Fallo al ejecutar la consulta en la l?nea" . __LINE__ . ": " . phpmkr_error($conn) . '<br>SQL: ' . $sSql);
	while ($row_rs = $rs->fetch_assoc())
	{$var_codigo_item=$row_rs['codigo'];}
	$hora=date("H:i:s");
	$fecha_actual=fecha_aplicacion($conn);
	$sSql="insert into secret_general values($var_secret,'$fecha_actual','$hora',
	$var_codigo_item,'$cedula',3,'$descripcion','$nombre_operador',$cod_empresa_sc)";
	phpmkr_query($sSql,$conn) 
	or die("Fallo al ejecutar la consulta en la l?nea" . __LINE__ . ": " . phpmkr_error($conn) . '<br>SQL: ' . $sSql);

	phpmkr_query("update codigos set secret = ".$var_secret."",$conn) 
	or die("Fallo al ejecutar la consulta en la l?nea" . __LINE__ . ": " . phpmkr_error($conn) . '<br>SQL: ' . $sSql);
}
/**
 * 
 * @todo Funcion regresa horas restadas
 * @author Jean Carlos Nuñez
 * @param time $inicio
 * @param time $fin
 * @return time
 *  
 */ 
function restar_horas($inicio, $fin)
{
	$dif=date("H:i:s", strtotime("00:00:00") + strtotime($fin) - strtotime($inicio) );
	return $dif;
}

/**
 * 
 * @todo Funcion busca las unidades agendadas para mantenimiento por hora,fecha
 * @author Jean Carlos Nuñez
 * @param time $var_hora
 * @param date $var_fecha
 * @param db $conn
 * @param int $var_tipo
 * @return string
 *  
 */ 
function buscar_hora_und($var_hora,$var_fecha,$conn,$var_tipo,$var_cod_suc) 
{
	$var_hora=conversion_hora24($var_hora);
	$var_fecha=$var_fecha;
	$var_unidades[]="";
	$unidades="";
	$sSql="select num_und from agenda_mantenimientos where hora ='".$var_hora."' and fecha = '".$var_fecha."' and tipo=$var_tipo 
	and estado = 1 and cod_suc = $var_cod_suc";
	//echo $sSql;
	//errores($sSql);
	$i=1;
	$rs=phpmkr_query($sSql,$conn) or die("Fallo al ejecutar la consulta en la linea" . __LINE__ . ": " . phpmkr_error($conn) . '<br>SQL: ' . $sSql);	
	$con = phpmkr_num_rows($rs);
	while ($row_rs = $rs->fetch_assoc())
	{
		
		if($con>1)
		{
			if($i==2){
			$unidades .= $row_rs['num_und'];}
			if($i==1){
				$unidades .= $row_rs['num_und']." y ";
			}
		}
		if($con==1)
		{
			$unidades .= $row_rs['num_und'];
		}
		$i=$i+1;
	}
	
	if($con==0){$unidades = "Ninguna";}
	return $unidades;
}

/**
 * 
 * @todo Funcion busca el color por mantenimiento asignado a la unidad
 * @author Jean Carlos Nuñez
 * @param time $var_hora
 * @param date $var_fecha
 * @param db $conn
 * @param int $var_tipo
 * @param int $var_cod_suc
 * @return string
 *  
 */ 
function buscar_color_hora($var_hora,$var_fecha,$conn,$var_tipo,$var_cod_suc) 
{
	$var_hora=conversion_hora24($var_hora);
	$var_fecha=$var_fecha;

	$sSql="select count(*) as contador from agenda_mantenimientos where hora ='".$var_hora."' and fecha = '".$var_fecha."' 
	and tipo = $var_tipo and estado = 1 and cod_suc = $var_cod_suc";
	$rs=phpmkr_query($sSql,$conn) 
	or die("Fallo al ejecutar la consulta en la linea" . __LINE__ . ": " . phpmkr_error($conn) . '<br>SQL: ' . $sSql);	
	while ($row_rs = $rs->fetch_assoc())
	{$var_contador = $row_rs['contador'];}

	if($var_contador==1){return "chartreuse";}
	if($var_contador==2){return "red";}
	if($var_contador==0){return "black";}
}
/**
 * 
 * @todo Funcion que determina si un ticket de siniestros esta duplicado en el sistema y no deja factora
 * @author Jean Carlos Nuñez
 * @param int $var_numero_ticket
 * @param int $var_num_ope
 * @param db $conn
 * @return boolean
 *  
 */ 
function tickets_duplicados_sini($var_numero_ticket,$var_num_ope,$conn) 
{
	$var_contador=0;
	
	$today=fecha_aplicacion($conn);
	$hora = date('H:i:s');

	$sSql="select count(*) as contador from siniestros_hist where codigo = ".$var_numero_ticket." 
	and num_ope =".$var_num_ope."";
	$rs=phpmkr_query($sSql,$conn) 
	or die("Fallo al ejecutar la consulta en la l?nea" . __LINE__ . ": " . phpmkr_error($conne) . '<br>SQL: ' . $sSql);	

	while ($row_rs = $rs->fetch_assoc())
	{
		$var_contador = $row_rs['contador'];
	}

	if($var_contador>0)
	{
		return true;
	}
	else
	{
		return false;
	}	
}
/**
 * 
 * @todo Funcion determinado si un ticket de renta diara esta duplicado
 * @author Jean Carlos Nuñez
 * @param int $var_numero_ticket
 * @param int $var_num_ope
 * @param string $var_num_und
 * @param int kilometraje
 * @param int $var_empresa
 * @param db conn
 * @return boolean
 *  
 */ 
function tickets_duplicados($var_numero_ticket,$var_num_ope,$var_num_und,$var_kilometraje,$var_empresa,$conn) 
{
	$var_contador=0;
	
	$today=fecha_aplicacion($conn);
	$hora = date('H:i:s');

	$sSql="select count(*) as contador from tickets where numero_ticket = ".$var_numero_ticket." 
	and num_ope =".$var_num_ope." and kilometros = ".$var_kilometraje." and empresa =".$var_empresa."";
	$rs=phpmkr_query($sSql,$conn) 
	or die("Fallo al ejecutar la consulta en la linea" . __LINE__ . ": " . phpmkr_error($conne) . '<br>SQL: ' . $sSql);

	

	while ($row_rs = $rs->fetch_assoc())
	{
		$var_contador = $row_rs['contador'];
	}

	if($var_contador>0)
	{
		phpmkr_query("insert into tickets_duplicados values('$var_num_und',$var_num_ope,$var_empresa,
		$var_numero_ticket,$var_kilometraje,'$today','$hora')",$conn)
		or die("Fallo al ejecutar la consulta en la linea" . __LINE__ . ": " . phpmkr_error($conn) . '<br>SQL: ' . $sSql);
		return true;
	}
	else{return false;}	
}
/**
 * 
 * @todo Funcion regresa la fecha del motor de base de datos
 * @author Jean Carlos Nuñez
 * @param db $conne
 * @return date
 *  
 */ 
function fecha_aplicacion_mysql($conne) 
{
	$rs=phpmkr_query("select curdate() as fecha",$conne) 
	or die("Fallo al ejecutar la consulta en la l?nea" . __LINE__ . ": " . phpmkr_error($conne) . '<br>SQL: ' . $sSql);
	while ($row_rs = $rs->fetch_assoc())
	{return $row_rs['fecha'];}		

}
/**
 * 
 * @todo Funcion regresa la fecha del sistema
 * @author Jean Carlos Nuñez
 * @param db $conne
 * @return date
 *  
 */  
function fecha_aplicacion($conne) 
{
	$rs=phpmkr_query("select fecha from fecha_aplicacion",$conne) 
	or die("Fallo al ejecutar la consulta en la l?nea" . __LINE__ . ": " . phpmkr_error($conne) . '<br>SQL: ' . $sSql);
	while ($row_rs = $rs->fetch_assoc())
	{return $row_rs['fecha'];}		

}
/**
 * 
 * @todo Funcion regresa la hora del motor de base de datos
 * @author Jean Carlos Nuñez
 * @param db $conne
 * @return time
 *  
 */ 
function hora_aplicacion($conne) 
{
	global $conn;
	$sSql="select curtime() as hora";
	$rs=phpmkr_query($sSql,$conn) or die("Fallo al ejecutar la consulta en la linea" . __LINE__ . ": " . phpmkr_error($conn) . '<br>SQL: ' . $sSql);
	while ($row_rs = $rs->fetch_assoc())
	{return $row_rs['hora'];}	
	//return date('H:i:s');
	
}
/**
 * 
 * @todo Funcion que regresa un mensaje en javascript
 * @author Jean Carlos Nuñez 
 * @return string
 *  
 */  
function validar_acciones_operadores()
{
	echo "<script>\n";
	echo "function validar_acciones_operador(valor,num_ope)\n";
	echo "{\n";
	echo "if(valor=='1'){alert('EL REGISTRO FUE INSERTADO CON EXITO!,CON ESTE NUMERO DE OPERADOR: '+num_ope);} \n"; 	
	echo "if(valor=='2'){alert('EL REGISTRO FUE MODIFICADO CON EXITO!');} \n"; 
	echo "if(valor=='3'){alert('EL REGISTRO FUE ELIMINADO CON EXITO!');} \n"; 		
	echo "	}\n";
	echo "</script>\n";
	
};

/**
 * 
 * @todo Funcion que regresa el nombre de la aplicacion con su version
 * @author Jean Carlos Nuñez
 * @return string
 *  
 */ 
 
function nombre_aplicacion(){
	return "Open Taxi OnLine, v6.7";
}

/**
 * 
 * @todo Funcion quita caracteres extranos de una cadena
 * @author Jean Carlos Nuñez
 * @param string $cadena
 * @return string
 *  
 */ 
function quitar_caracteres_raros($cadena){
   $caracteres = "-";
   $caracteres = explode(' ',$caracteres);
   $nchar      = count($caracteres);
   $base       = 0;
   while($base<$nchar){
      $cadena = str_replace($caracteres[$base],'',$cadena);
      $base++;
   }
   return $cadena;
}
/**
 * 
 * @todo Funcion quita caracteres extranos de una cadena
 * @author Jean Carlos Nuñez
 * @param string $cadena
 * @return string
 *  
 */
function quitar_caracteres_raros2($cadena){
   $caracteres = "'";
   $caracteres = explode(' ',$caracteres);
   $nchar      = count($caracteres);
   $base       = 0;
   while($base<$nchar){
      $cadena = str_replace($caracteres[$base],'',$cadena);
      $base++;
   }
   return $cadena;
}
/**
 * 
 * @todo Funcion quita caracteres extranos de una cadena
 * @author Jean Carlos Nuñez
 * @param string $var_ip
 * @param time $var_hora
 * @param date $var_fecha
 * @return mail
 *  
 */
function envio_coreo($var_ip,$var_hora,$var_fecha)
{
	$to      = 'jeancarlosn@hotmail.com';
	$subject = 'IP QUE ESTA VIENDO LA PAGINA TAXICONTROL';
	$message = 'Direccion Ip '.$var_ip."\r\n"."Fecha: ".fecha($var_fecha)."\r\n"."Hora: ".conversion_hora($var_hora)."\r\n";
	$headers = 'From: administrador@compuzulia.net' . "\r\n".
	    'Reply-To: administrador@compuzulia.net' . "\r\n" .
	    'X-Mailer: PHP/' . phpversion();		
	mail($to, $subject, $message, $headers);
}

/**
 * 
 * @todo Funcion deprecada de estilo de la aplicacion
 * @author Jean Carlos Nuñez
 * @return string
 *  
 */
function estilo()
{
	global $conn; 
	$rs = mysqli_query($conn,"select * from operaciones_generales ");
	while ($row_rs = mysqli_fetch_array($rs))
	{
		$var_icono=$row_rs['icono'];$var_hoja_estilo=$row_rs['hoja_estilo'];
	}

echo $var_icono."\n";
echo $var_hoja_estilo."\n";
}

/**
 * 
 * @todo Funcion resta horas
 * @author Jean Carlos Nuñez
 * @param time $horaini
 * @param time $horafin
 * @return string
 *  
 */
function RestarHoras($horaini,$horafin)
{
	$horai=substr($horaini,0,2);
	$horaf=substr($horafin,0,2);
	$hora_total = $horaf - $horai;
	return $hora_total;
}
/**
 * 
 * @todo Funcion pasa datos a un frame
 * @author Jean Carlos Nuñez
 * @return string
 *  
 */
function pasar_al_iframe()
{
	echo " <script type='text/javascript'>\n";
	echo "function pasar_al_iframe(url){\n";	
    echo " $('#myFrame').attr('src', 'url');\n";
    
	echo " }\n";
	echo "</script>\n";
		
}
/**
 * 
 * @todo Funcion convierte un numero al nombre del mes del ano
 * @author Jean Carlos Nuñez
 * @param int $mes
 * @return string
 *  
 */
function convertir_mes($mes)
{
	if($mes==1){return "Enero";}
	if($mes==2){return "Febrero";}
	if($mes==3){return "Marzo";}
	if($mes==4){return "Abril";}
	if($mes==5){return "Mayo";}
	if($mes==6){return "Junio";}
	if($mes==7){return "Julio";}
	if($mes==8){return "Agosto";}
	if($mes==9){return "Septiembre";}
	if($mes==10){return "Octubre";}
	if($mes==11){return "Noviembre";}
	if($mes==12){return "Diciembre";}
}
/**
 * 
 * @todo Funcion cabecera de una funcion de javascript
 * @author Jean Carlos Nuñez
 * @return string
 *  
 */
function teclas_1()
{
	echo "<script type='text/javascript'>\n";
	echo "var kb = new kb_shortcut();\n";
}
/**
 * 
 * @todo Funcion de acciones con el teclado atraves de javascript
 * @author Jean Carlos Nuñez
 * @param int $tecla
 * @param string $archivo
 * @return string
 *  
 */
function teclas_2($tecla,$archivo)
{

	echo "kb.add(['".$tecla."'], function()\n";
	echo "{\n";
	echo "window.location = '".$archivo."'\n";;
	echo "});\n";
}
/**
 * 
 * @todo Funcion fin de la funcion de javascript
 * @author Jean Carlos Nuñez
 * @return string
 *  
 */
function teclas_3()
{

	echo "</script>\n";
}
/**
 * 
 * @todo Funcion tecla rapida en javascript para mostrar ventanas de los productos
 * @author Jean Carlos Nuñez
 * @return string
 *  
 */
function teclas_buscar_productos()
{
	echo "<script type='text/javascript'>\n";
	echo "var kb = new kb_shortcut();\n";
	echo "kb.add(['Ctrl','X'], function()\n";
	echo "{\n";
	echo "openproductos();";;
	echo "});\n";
	echo "</script>\n";
}
/**
 * 
 * @todo Funcion tecla rapida en javascript para mostrar ventanas pagos varios
 * @author Jean Carlos Nuñez
 * @return string
 *  
 */
function teclas_pago_manual()
{
	echo "<script type='text/javascript'>\n";
	echo "var kb = new kb_shortcut();\n";
	echo "kb.add(['F10'], function()\n";
	echo "{\n";
	echo "openwindow();";;
	echo "});\n";
	echo "</script>\n";
}
/**
 * 
 * @todo Funcion tecla rapida para venta de inscripciones y otros
 * @author Jean Carlos Nuñez
 * @return string
 *  
 */
function teclas_nuevas()
{
	echo "<script type='text/javascript'>\n";
	echo "var kb = new kb_shortcut();\n";
	echo "kb.add(['F7'], function()\n";
	echo "{\n";
	echo "openwindow_ins_asi();";;
	echo "});\n";
	echo "</script>\n";
}
/**
 * 
 * @todo Funcion tecla rapida para salir a la ventana principal de la aplicacion
 * @author Jean Carlos Nuñez
 * @return string
 *  
 */
function teclas_salir()
{
	echo "<script type='text/javascript'>\n";
	echo "var kb = new kb_shortcut();\n";
	echo "kb.add(['F8'], function()\n";
	echo "{\n";
	echo "window.location = 'principal.php'\n";;
	echo "});\n";
	echo "</script>\n";
}
/**
 * 
 * @todo Funcion tecla rapida para abrir modulo de operadores
 * @author Jean Carlos Nuñez
 * @return string
 *  
 */
function operador_pagos()
{
	echo "<script type='text/javascript'>\n";
	echo "var kb = new kb_shortcut();\n";
	echo "kb.add(['F4'], function()\n";
	echo "{\n";
	echo "window.location = 'javascript: openwindow_ope();'\n";;
	echo "});\n";
	echo "</script>\n";
}
/**
 * 
 * @todo Funcion tecla rapida para el agendamiento de mantenimiento
 * @author Jean Carlos Nuñez
 * @return string
 *  
 */
function agendar()
{
	echo "<script type='text/javascript'>\n";
	echo "var kb = new kb_shortcut();\n";
	echo "kb.add(['F9'], function()\n";
	echo "{\n";
	echo "window.location = 'javascript: openwindow_man();'\n";;
	echo "});\n";
	echo "</script>\n";
}
/**
 * 
 * @todo Funcion tecla rapida para salir de las ventanas emergentes de toda la aplicacion
 * @author Jean Carlos Nuñez
 * @return string
 *  
 */
function teclas_cerrar()
{
	echo "<script type='text/javascript'>\n";
	echo "var kb = new kb_shortcut();\n";
	echo "kb.add(['F8'], function()\n";
	echo "{\n";
	echo "window.close()\n";;
	echo "});\n";
	echo "</script>\n";
}
/**
 * 
 * @todo Funcion tecla rapida para refrescar la taquilla
 * @author Jean Carlos Nuñez
 * @return string
 *  
 */
function recargar()
{
	echo "<script type='text/javascript'>\n";
	echo "var kb = new kb_shortcut();\n";
	echo "kb.add(['F5'], function()\n";
	echo "{\n";
	echo "window.location = 'taquilla.php'\n";;
	echo "});\n";
	echo "</script>\n";
}
/**
 * 
 * @todo Funcion que regresa el codigo de empresa por usuarios
 * @author Jean Carlos Nuñez
 * @param int $var_cod_usu
 * @return int
 *  
 */
function empresa($var_cod_usu)
{
	global $conn; 
	$rs=phpmkr_query("select cod_empresa from usuarios where  codigo = $var_cod_usu ",$conn) 
	or die("Fallo al ejecutar la consulta en la l?nea" . __LINE__ . ": " . phpmkr_error($conn) . '<br>SQL: ' . $sSql);
	while ($row_rs = $rs->fetch_assoc())
	{return $row_rs['cod_empresa'];}
}


/**
 * 
 * @todo Funcion que busca productos por codigo secundario para las salidas
 * @author Jean Carlos Nuñez
 * @param string $var_cod_sec
 * @return array
 *  
 */
  
function buscar_productos_codigo_sec_pro_jq($var_cod_sec,$var_cod_usu)
{
	global $conn; 
	$sSql="select p.referencia,p.descripcion,p.unidad,p.codigo_secundario,p.codigo,c.cantidad,c.monto,c.cod_pro,c.cod_dep,c.factura from productos p,central c 
	where c.cod_pro = p.codigo and p.codigo_secundario = '$var_cod_sec' 
	and c.cod_dep in (select cod_dep from usuarios_depositos where cod_usu = ".$var_cod_usu.") order by c.cantidad limit 1";
	
	$rs=phpmkr_query($sSql,$conn) or die("Fallo al ejecutar la consulta en la linea" . __LINE__ . ": " . phpmkr_error($conn) . '<br>SQL: ' . $sSql);
	$con = phpmkr_num_rows($rs);
	if($con>0)
	{
		$row_rs = phpmkr_fetch_row($rs);
		$var_resultado[]=$row_rs;
	}else{$row_rs=array('0'=>0);}
	
	return $row_rs;
}


/**
 * 
 * @todo Funcion que genera una funcion javascript para la busqueda de los productos
 * @author Jean Carlos Nuñez
 * @return string
 *  
 */
function buscar_productos_codigo_sec_pro()
{
	global $conn; 
	$rs = mysqli_query($conn,"select p.referencia,p.descripcion,p.unidad,p.codigo_secundario,p.codigo from productos p Order by p.codigo");

	echo "<script>\n";
	echo "function buscar_productos_codigo_sec_pro()\n";
	echo "{\n";
	echo "var doc = document.form1; \n";
	echo "var bandera=0;\n"; 
	echo "var var_codigo_sec = doc.codigo_sec.value; \n";
	echo "var_codigo_sec=var_codigo_sec.toUpperCase(); \n"; 
	while ($row_rs = mysqli_fetch_array($rs))
	{
		$var_referencia=quitar_caracteres_raros2($row_rs['referencia']);
		$var_descripcion=$row_rs['descripcion'];
		$var_unidad=$row_rs['unidad']; 
		$var_cod_pro=$row_rs['codigo'];		
		$var_codigo_secundario=strtoupper($row_rs['codigo_secundario']);
	echo "	if(var_codigo_sec=='".$var_codigo_secundario."')\n";
	echo "	{ \n";
	echo "		doc.codigo_sec.value='".$var_codigo_secundario."'; \n";
	echo "		doc.unidad.value='".$var_unidad."'; \n";
	echo "		doc.descripcion_pro.value='".$var_descripcion."'; \n";
	echo "		doc.codigo.value='".$var_cod_pro."'; \n";
	echo " 		bandera=1;\n";
	echo "		doc.cantidad.focus(); \n";
	echo "	}\n"; }
	echo "	if(bandera=='0'){\n";
	echo "  alert('NO EXISTE PRODUCTO'); doc.codigo_sec.focus();\n";
	echo "	}\n"; 
	echo"
	}
	</script>	
	\n";		
};
/**
 * 
 * @todo Funcion que genera una funcion en javascript para buscar productos por codigo secundario
 * @author Jean Carlos Nuñez
 * @return string
 *  
 */
function buscar_productos_codigo_sec()
{ 
	global $conn; 
	$rs = mysqli_query($conn,"select c.cod_pro,p.referencia,p.descripcion,p.unidad,c.cantidad,c.monto,c.cod_dep,p.codigo_secundario,c.factura from productos p,central c where p.codigo = c.cod_pro and c.cod_dep=1 and p.codigo_secundario <> '-' order by c.cod_pro,c.cantidad");

	echo "<script>\n";
	echo "function buscar_productos_codigo_sec()\n";
	echo "{\n";
	echo "var doc = document.form1; \n";
	echo "var bandera=0;\n"; 
	echo "var var_codigo_sec = doc.codigo_sec.value; \n";
	echo "var_codigo_sec=var_codigo_sec.toUpperCase(); \n"; while ($row_rs = mysqli_fetch_array($rs)){$var_referencia=quitar_caracteres_raros2($row_rs['referencia']);$var_descripcion=$row_rs['descripcion'];$var_factura=$row_rs['factura'];
	$var_unidad=$row_rs['unidad']; $var_cantidad=$row_rs['cantidad']; $var_monto=$row_rs['monto']; $var_cod_pro=$row_rs['cod_pro'];$var_cod_dep=$row_rs['cod_dep'];$var_codigo_secundario=strtoupper($row_rs['codigo_secundario']);
	echo "	if(var_codigo_sec=='".$var_codigo_secundario."')\n";
	echo "	{ \n";
	echo "		doc.cod_pro.value='".$var_cod_pro."'; \n";
	echo "		doc.unidad.value='".$var_unidad."'; \n";
	echo "		doc.descripcion_pro.value='".$var_descripcion."'; \n";
	echo "		doc.cantidad_act.value='".$var_cantidad."'; \n";
	echo "		doc.monto_act.value='".$var_monto."'; \n";
	echo "		doc.cod_dep_pro.value='".$var_cod_dep."'; \n";
	echo "		doc.codigo.value='".$var_referencia."'; \n";
	echo "		doc.factura.value='".$var_factura."'; \n";	
	echo " 		bandera=1;\n";
	echo "		doc.cantidad.focus(); \n";
	echo "	}\n"; }
	echo "	if(bandera=='0'){\n";
	echo "  alert('NO EXISTE PRODUCTO'); doc.codigo_sec.focus();\n";
	echo "	}\n"; 
	echo"
	}
	</script>	
	\n";		
};
/**
 * 
 * @todo Funcion que genera un datalist de los productos para que interactue con el usuario
 * @author Jean Carlos Nuñez
 * @param int $var_tipo
 * @return string
 *  
 */
function crear_datalist($var_tipo)
{
	global $conn;	
	
	if($var_tipo==1)
	{
		echo "<datalist id='pro1' name='pro1'>\n";
		$rs = mysqli_query($conn,"select * from productos order by descripcion");	
		while ($row_rs = mysqli_fetch_array($rs))
		{
			$var_descripcion=utf8_encode(substr($row_rs['codigo_secundario'],0,100))." - ".utf8_encode(substr($row_rs['descripcion'],0,100));	
			echo "<option value='".$row_rs['descripcion']."'>".$var_descripcion."</option>\n";		
		}
		echo "</datalist>\n";
	}
	if($var_tipo==2)
	{
		echo "<datalist id='pro2' name='pro2'>\n";
		$rs = mysqli_query($conn,"select * from productos order by descripcion");	
		while ($row_rs = mysqli_fetch_array($rs))
		{
			$var_descripcion=utf8_encode(substr($row_rs['codigo_secundario'],0,100))." - ".utf8_encode(substr($row_rs['descripcion'],0,100));	
			echo "<option value='".$row_rs['codigo_secundario']."'>".$var_descripcion."</option>\n";		
		}
		echo "</datalist>\n";
	}
	if($var_tipo==3)
	{
		echo "<datalist id='pro3' name='pro3'>\n";
		$rs = mysqli_query($conn,"select * from productos order by descripcion");	
		while ($row_rs = mysqli_fetch_array($rs))
		{
			$var_descripcion=utf8_encode(substr($row_rs['referencia'],0,100));	
			echo "<option value='".$row_rs['referencia']."'>".$var_descripcion."</option>\n";		
		}
		echo "</datalist>\n";
	}
	
}

/**
 * 
 * @todo Funcion que genera un datalist de los depositos
 * @author Jean Carlos Nuñez
 * @param int $var_tipo
 * @return string
 *  
 */
function crear_datalist_depositos()
{
	global $conn;	
	echo "<script>\n";
	echo "function crear_datalist_depositos(valor,desc)\n";
	echo "{\n";
	echo "var doc = document.form1; \n";
	echo "if(valor==6)\n";	
	echo "{\n";
	
	echo "var deleteFile = document.getElementById('buscar');\n";
	echo "var contenedor = document.getElementById('contenedor')\n";
	echo "contenedor.removeChild(deleteFile);\n";		
	echo "caja = document.createElement('input');\n";	
	echo "data = document.createElement('datalist');\n";
	echo "opt = document.createElement('option');\n";
	echo "caja.setAttribute('name','buscar');\n";
	echo "caja.setAttribute('list','pro1');\n";
	echo "caja.setAttribute('id','buscar');\n";
	echo "caja.setAttribute('class','input-block-level');\n";
	echo "caja.setAttribute('value','');\n";
	
	echo "document.getElementById('contenedor').appendChild(caja);\n";	
	echo "doc.buscar.focus(); \n";
	echo "doc.condicion.value=0; \n";
	echo"}\n";
	

	
	echo "if(valor==7)\n";	
	echo "{\n";
	
	echo "var deleteFile = document.getElementById('buscar');\n";
	echo "var contenedor = document.getElementById('contenedor')\n";
	echo "contenedor.removeChild(deleteFile);\n";		
	echo "caja = document.createElement('input');\n";	
	echo "data = document.createElement('datalist');\n";
	echo "opt = document.createElement('option');\n";
	echo "caja.setAttribute('name','buscar');\n";
	echo "caja.setAttribute('list','pro2');\n";
	echo "caja.setAttribute('id','buscar');\n";
	echo "caja.setAttribute('class','input-block-level');\n";
	echo "caja.setAttribute('value','');\n";
	
	echo "document.getElementById('contenedor').appendChild(caja);\n";	
	echo "doc.buscar.focus(); \n";
	echo "doc.condicion.value=0; \n";
	echo"}\n";
	
	echo "if(valor==8)\n";	
	echo "{\n";
	
	echo "var deleteFile = document.getElementById('buscar');\n";
	echo "var contenedor = document.getElementById('contenedor')\n";
	echo "contenedor.removeChild(deleteFile);\n";		
	echo "caja = document.createElement('input');\n";	
	echo "data = document.createElement('datalist');\n";
	echo "opt = document.createElement('option');\n";
	echo "caja.setAttribute('name','buscar');\n";
	echo "caja.setAttribute('list','pro3');\n";
	echo "caja.setAttribute('id','buscar');\n";
	echo "caja.setAttribute('class','input-block-level');\n";
	echo "caja.setAttribute('value','');\n";
	
	echo "document.getElementById('contenedor').appendChild(caja);\n";	
	echo "doc.buscar.focus(); \n";
	echo "doc.condicion.value=0; \n";
	echo"}\n";
	
	echo "if(valor==5)\n";	
	echo "{\n";
	$rs = mysqli_query($conn,"select * from depositos where estado=1 order by descripcion ");
	echo "var deleteFile = document.getElementById('buscar');\n";;
	echo "var contenedor = document.getElementById('contenedor')\n";;
	echo "contenedor.removeChild(deleteFile);\n";		
	echo "caja = document.createElement('select');\n";	
	echo "var objSelect = caja;\n";	
	echo "caja.setAttribute('name','buscar');\n";
	echo "caja.setAttribute('id','buscar');\n";while ($row_rs = mysqli_fetch_array($rs)){$var_descripcion=$row_rs['descripcion'];
	echo "var Item = new Option('".$var_descripcion."','".$var_descripcion."');\n";	
	echo "objSelect.options[objSelect.length] = Item;\n";}	
	echo "document.getElementById('contenedor').appendChild(caja);\n";
	echo "doc.condicion.value=5; \n";
	
	echo"
	}\n";
		
	echo "\n
	}
	</script>	
	\n";
		
};
/**
 * 
 * @todo Funcion que genera una funcion en javascript de los productos existente en el inventario
 * @author Jean Carlos Nuñez
 * @return string
 *  
 */
function buscar_productos_referencia()
{
	global $conn; 
	$rs = mysqli_query($conn,"select c.cod_pro,p.referencia,p.descripcion,p.unidad,c.cantidad,c.monto,c.cod_dep,p.codigo_secundario,c.factura from productos p,central c where p.codigo = c.cod_pro and c.cod_dep=1 and p.referencia <> '-'");

	echo "<script>\n";
	echo "function buscar_productos_referencia()\n";
	echo "{\n";
	echo "var doc = document.form1; \n"; 
	echo "var bandera=0;\n"; 
	echo "var var_codigo = doc.codigo.value; \n";while ($row_rs = mysqli_fetch_array($rs)){$var_referencia=quitar_caracteres_raros2($row_rs['referencia']);$var_descripcion=$row_rs['descripcion'];$var_factura=$row_rs['factura'];
	$var_unidad=$row_rs['unidad']; $var_cantidad=$row_rs['cantidad']; $var_monto=$row_rs['monto']; $var_cod_pro=$row_rs['cod_pro'];$var_cod_dep=$row_rs['cod_dep'];$var_codigo_secundario=$row_rs['codigo_secundario'];
	echo "	if(var_codigo=='".$var_referencia."')\n";
	echo "	{ \n";
	echo "		doc.cod_pro.value='".$var_cod_pro."'; \n";
	echo "		doc.unidad.value='".$var_unidad."'; \n";
	echo "		doc.descripcion_pro.value='".$var_descripcion."'; \n";
	echo "		doc.cantidad_act.value='".$var_cantidad."'; \n";
	echo "		doc.monto_act.value='".$var_monto."'; \n";
	echo "		doc.cod_dep_pro.value='".$var_cod_dep."'; \n";
	echo "		doc.codigo_sec.value='".$var_codigo_secundario."'; \n";	
	echo "		doc.factura.value='".$var_factura."'; \n";
	echo " 		bandera=1;\n";
	echo "		doc.cantidad.focus(); \n";
	echo "	}\n"; }
	echo "	if(bandera=='0'){\n";
	echo "  alert('NO EXISTE PRODUCTO'); doc.codigo.focus();\n";
	echo "	}\n";
	echo"
	}
	</script>	
	\n";		
};

/**
 * 
 * @todo Funcion que genera una funcion en javascript para destruir objetos en la pagina
 * @author Jean Carlos Nuñez
 * @return string
 *  
 */
 
function crear_destruir_marca_departemento_clase()
{
	global $conn;	
	echo "<script>\n";
	echo "function crear_destruir_marca_departemento_clase(valor,desc)\n";
	echo "{\n";
	echo "var doc = document.form1; \n"; 
	echo "if(valor==4)\n";	
	echo "{\n";
	$rs = mysqli_query($conn,"select * from departamentos order by descripcion ");
	echo "var deleteFile = document.getElementById('buscar');\n";;
	echo "var contenedor = document.getElementById('contenedor')\n";;
	echo "contenedor.removeChild(deleteFile);\n";		
	echo "caja = document.createElement('input');\n";	
	echo "caja.setAttribute('name','buscar');\n";
	echo "caja.setAttribute('id','buscar');\n";		
	echo "document.getElementById('contenedor').appendChild(caja);\n";
	echo "doc.buscar.focus(); \n";
	echo "doc.condicion.value=0; \n";
	echo"}\n";
	
	echo "if(valor==1)\n";	
	echo "{\n";
	$rs = mysqli_query($conn,"select * from departamentos order by descripcion ");
	echo "var deleteFile = document.getElementById('buscar');\n";;
	echo "var contenedor = document.getElementById('contenedor')\n";;
	echo "contenedor.removeChild(deleteFile);\n";		
	echo "caja = document.createElement('select');\n";	
	echo "var objSelect = caja;\n";	
	echo "caja.setAttribute('name','buscar');\n";
	echo "caja.setAttribute('id','buscar');\n";while ($row_rs = mysqli_fetch_array($rs)){$var_descripcion=$row_rs['descripcion'];
	echo "var Item = new Option('".$var_descripcion."','".$var_descripcion."');\n";	
	echo "objSelect.options[objSelect.length] = Item;\n";	
	echo "document.getElementById('contenedor').appendChild(caja);\n";
	echo "doc.condicion.value=1; \n";
	}
	echo"
	}";
	
	echo "if(valor==2)\n";	
	echo "{\n";
	$rs = mysqli_query($conn,"select * from marcas order by descripcion ");
	echo "var deleteFile = document.getElementById('buscar');\n";;
	echo "var contenedor = document.getElementById('contenedor')\n";;
	echo "contenedor.removeChild(deleteFile);\n";		
	echo "caja = document.createElement('select');\n";	
	echo "var objSelect = caja;\n";	
	echo "caja.setAttribute('name','buscar');\n";
	echo "caja.setAttribute('id','buscar');\n";while ($row_rs = mysqli_fetch_array($rs)){$var_descripcion=$row_rs['descripcion'];
	echo "var Item = new Option('".$var_descripcion."','".$var_descripcion."');\n";	
	echo "objSelect.options[objSelect.length] = Item;\n";	
	echo "document.getElementById('contenedor').appendChild(caja);\n";
	echo "doc.condicion.value=2; \n";
	}
	echo"
	}\n";
	
	echo "if(valor==3)\n";	
	echo "{\n";
	$rs = mysqli_query($conn,"select * from clases order by descripcion ");
	echo "var deleteFile = document.getElementById('buscar');\n";;
	echo "var contenedor = document.getElementById('contenedor')\n";;
	echo "contenedor.removeChild(deleteFile);\n";		
	echo "caja = document.createElement('select');\n";	
	echo "var objSelect = caja;\n";	
	echo "caja.setAttribute('name','buscar');\n";
	echo "caja.setAttribute('id','buscar');\n";while ($row_rs = mysqli_fetch_array($rs)){$var_descripcion=$row_rs['descripcion'];
	echo "var Item = new Option('".$var_descripcion."','".$var_descripcion."');\n";	
	echo "objSelect.options[objSelect.length] = Item;\n";	
	echo "document.getElementById('contenedor').appendChild(caja);\n";
	echo "doc.condicion.value=3; \n";
	}
	echo"
	}\n";
	
	echo "if(valor==5)\n";	
	echo "{\n";
	$rs = mysqli_query($conn,"select * from depositos where estado=1 order by descripcion ");
	echo "var deleteFile = document.getElementById('buscar');\n";;
	echo "var contenedor = document.getElementById('contenedor')\n";;
	echo "contenedor.removeChild(deleteFile);\n";		
	echo "caja = document.createElement('select');\n";	
	echo "var objSelect = caja;\n";	
	echo "caja.setAttribute('name','buscar');\n";
	echo "caja.setAttribute('id','buscar');\n";while ($row_rs = mysqli_fetch_array($rs)){$var_descripcion=$row_rs['descripcion'];
	echo "var Item = new Option('".$var_descripcion."','".$var_descripcion."');\n";	
	echo "objSelect.options[objSelect.length] = Item;\n";	
	echo "document.getElementById('contenedor').appendChild(caja);\n";
	echo "doc.condicion.value=5; \n";
	}
	echo"
	}\n";
		
	echo "\n
	}
	</script>	
	\n";
		
};

/**
 * 
 * @todo Funcion busca los grupos de diarios en la base de datos
 * @author Jean Carlos Nuñez
 * @return string
 *  
 */
 
function buscar_diario()
{
	global $conn; 
	$rs = mysqli_query($conn,"select * from grupos_diarios ");

	echo "<script>\n";
	echo "function buscar_diario()\n";
	echo "{\n";
	echo "var doc = document.form1; \n"; 
	echo "var var_gru_dia = doc.gru_dia.value; \n";while ($row_rs = mysqli_fetch_array($rs)){$var_codigo=$row_rs['codigo'];$var_lunes=$row_rs['lunes'];
	echo "	if(var_gru_dia=='".$var_codigo."')\n";
	echo "	{ \n";
	echo "		doc.monto_diario.value='".$var_lunes."'; \n";
	echo "	}\n"; }
	echo"
	}
	</script>	
	\n";
		
};
/**
 * 
 * @todo Funcion que genera una funcion en javascript para buscar productos en el inventario atraves del codigo secundario
 * @author Jean Carlos Nuñez
 * @return string
 *  
 */
function buscar_codigo_productos_secundario_traslado()
{
	global $conn; 
	$rs = mysqli_query($conn,"select p.codigo,p.descripcion,p.codigo_secundario,c.cantidad,
		d.descripcion as nombre_dep,c.cod_dep,c.monto,c.factura 
			from productos p,central c,depositos d 
	where c.cod_pro = p.codigo and d.codigo = c.cod_dep and p.estado = 1");
	echo "<script>\n";
	echo "function buscar_codigo_productos_secundario_traslado()\n";
	echo "{\n";
	echo "var doc = document.form1; \n";
	echo "var bandera = 0; \n"; 	
	echo "var var_codigo_sec = doc.codigo_sec.value; \n";	
	echo "if(var_codigo_sec!=''){";while ($row_rs = mysqli_fetch_array($rs)){$var_monto=$row_rs['monto'];$var_codigo_secundario=$row_rs['codigo_secundario'];$var_nombre_dep=$row_rs['nombre_dep'];$var_cod_dep=$row_rs['cod_dep'];$var_codigo=$row_rs['codigo']; $var_descripcion=$row_rs['descripcion']; $var_cantidad=$row_rs['cantidad'];$var_factura=$row_rs['factura'];
	echo "	if(var_codigo_sec=='".$var_codigo_secundario."')\n";
	echo "	{ \n";
	echo "		doc.codigo.value='".$var_codigo."'; \n";
	echo "		doc.descripcion.value='".$var_descripcion."'; \n";
	echo "		doc.codigo.value='".$var_codigo."'; \n";
	echo "		doc.cantidad_act.value='".$var_cantidad."'; \n";
	echo "		doc.cod_dep2.value='".$var_cod_dep."'; \n";
	echo "		doc.nom_dep2.value='".$var_nombre_dep."'; \n";
	echo "		doc.monto.value='".$var_monto."'; \n";
	echo "		doc.factura.value='".$var_factura."'; \n";
	echo "		doc.cod_dep.focus(); \n";
	echo "		bandera=1; \n";				
	echo "	}\n"; }	
	echo "	if(bandera==0)\n";
	echo "	{ \n";	
	echo " 		alert('ESTE CODIGO NO EXISTE');doc.codigo_sec.value=''; doc.codigo_sec.focus();\n";
	echo "	}}\n";
	echo"
	}
	</script>	
	\n";		
};
/**
 * 
 * @todo Funcion que genera una funcion en javascript para listar los pruductos por codigo secundario
 * @author Jean Carlos Nuñez
 * @return string
 *  
 */
function buscar_codigo_productos_secundario()
{
	global $conn; 
	$rs = mysqli_query($conn,"select * from productos where estado = 1");
	echo "<script>\n";
	echo "function buscar_codigo_productos_secundario()\n";
	echo "{\n";
	echo "var doc = document.form1; \n";
	echo "var bandera = 0; \n"; 	
	echo "var var_codigo_sec = doc.codigo_sec.value; \n";while ($row_rs = mysqli_fetch_array($rs)){$var_codigo_secundario=$row_rs['codigo_secundario']; $var_codigo=$row_rs['codigo']; $var_descripcion=$row_rs['descripcion']; $var_referencia=quitar_caracteres_raros2($row_rs['referencia']);	
	echo "if(var_codigo_sec!=''){";
	echo "	if(var_codigo_sec=='".$var_codigo_secundario."')\n";
	echo "	{ \n";
	echo "		doc.codigo.value='".$var_codigo."'; \n";
	echo "		doc.descripcion.value='".$var_descripcion."'; \n";
	echo "		bandera=1; \n";		
	echo "	}}\n"; }
	echo "if(var_codigo_sec!=''){";
	echo "	if(bandera==0)\n";
	echo "	{ \n";	
	echo " 		alert('ESTE CODIGO NO EXISTE');doc.codigo_sec.value=''; doc.codigo_sec.focus();\n";
	echo "	}}\n";
	echo"
	}
	</script>	
	\n";		
};
/**
 * 
 * @todo Funcion que genera una funcion en javascript para listar los pruductos por codigo secundario
 * @author Jean Carlos Nuñez
 * @return string
 *  
 */
function buscar_codigo_productos_secuendario()
{
	global $conn; 
	$rs = mysqli_query($conn,"select * from productos where estado = 1");
	echo "<script>\n";
	echo "function buscar_codigo_productos_secuendario()\n";
	echo "{\n";
	echo "var doc = document.form1; \n";	
	echo "var var_codigo_sec2 = doc.codigo_sec2.value; \n";
	echo "var var_codigo_sec = doc.codigo_sec.value; \n";while ($row_rs = mysqli_fetch_array($rs)){$var_codigo_secundario=$row_rs['codigo_secundario'];
	echo "if(var_codigo_sec2!=var_codigo_sec){";
	echo "	if(var_codigo_sec=='".$var_codigo_secundario."')\n";
	echo "	{ \n";
	echo "		alert('NO PUEDE REPETIR EL CODIGO DEL PRODUCTO'); doc.codigo_sec.value=''; doc.codigo_sec.focus();";	
	echo "	}}\n"; }	
	echo"
	}
	</script>	
	\n";		
};
/**
 * 
 * @todo Funcion que genera una funcion en javascript plas clases
 * @author Jean Carlos Nuñez
 * @return string
 *  
 */
function buscar_clases()
{
	global $conn; 
	$rs = mysqli_query($conn,"select * from clases ");

	echo "<script>\n";
	echo "function buscar_clases()\n";
	echo "{\n";
	echo "var doc = document.form1; \n";
	echo "var bandera = 0; \n"; 
	echo "var var_cod_cla = doc.cod_cla.value; \n";while ($row_rs = mysqli_fetch_array($rs)){$var_codigo=$row_rs['codigo']; $var_descripcion=$row_rs['descripcion'];
	echo "	if(var_cod_cla==''){bandera=1;}\n";	
	echo "	if(var_cod_cla=='".$var_codigo."')\n";
	echo "	{ \n";
	echo "		doc.clase.value='".$var_descripcion."'; \n";
	echo "		bandera=1; \n";
	echo "	}\n"; }
	echo "	if(bandera==0)\n";
	echo "	{ \n";
	echo " 		doc.cod_cla.value=''; doc.cod_cla.focus(); doc.clase.value='';\n";
	echo " 		alert('ESTE CODIGO NO EXISTE')";
	echo "	}\n";
	
	echo"
	}
	</script>	
	\n";
		
};
/**
 * 
 * @todo Funcion que genera una funcion en javascript para listar las marcas
 * @author Jean Carlos Nuñez
 * @return string
 *  
 */
function buscar_marca()
{
	global $conn; 
	$rs = mysqli_query($conn,"select * from marcas ");

	echo "<script>\n";
	echo "function buscar_marca()\n";
	echo "{\n";
	echo "var doc = document.form1; \n";
	echo "var bandera = 0; \n"; 
	echo "var var_cod_mar = doc.cod_mar.value; \n";while ($row_rs = mysqli_fetch_array($rs)){$var_codigo=$row_rs['codigo']; $var_descripcion=$row_rs['descripcion'];
	echo "	if(var_cod_mar==''){bandera=1;}\n";
	echo "	if(var_cod_mar=='".$var_codigo."')\n";
	echo "	{ \n";
	echo "		doc.marca.value='".$var_descripcion."'; \n";
	echo "		bandera=1; \n";
	echo "	}\n"; }
	echo "	if(bandera==0)\n";
	echo "	{ \n";
	echo " 		doc.cod_mar.value=''; doc.cod_mar.focus(); doc.marca.value='';\n";
	echo " 		alert('ESTE CODIGO NO EXISTE')";
	echo "	}\n";
	
	echo"
	}
	</script>	
	\n";
		
};
/**
 * 
 * @todo Funcion que genera una funcion en javascript para listar los departamento
 * @author Jean Carlos Nuñez
 * @return string
 *  
 */
function buscar_departamentos()
{
	global $conn; 
	$rs = mysqli_query($conn,"select * from departamentos ");

	echo "<script>\n";
	echo "function buscar_departamentos()\n";
	echo "{\n";
	echo "var doc = document.form1; \n";
	echo "var bandera = 0; \n"; 
	echo "var var_cod_dep = doc.cod_dep.value; \n";while ($row_rs = mysqli_fetch_array($rs)){$var_codigo=$row_rs['codigo']; $var_descripcion=$row_rs['descripcion'];
	echo "	if(var_cod_dep==''){bandera=1;}\n";	
	echo "	if(var_cod_dep=='".$var_codigo."')\n";
	echo "	{ \n";
	echo "		doc.departamento.value='".$var_descripcion."'; \n";
	echo "		bandera=1; \n";
	echo "	}\n"; }
	echo "	if(bandera==0)\n";
	echo "	{ \n";
	echo " 		doc.cod_dep.value=''; doc.cod_dep.focus(); doc.departamento.value='';\n";
	echo " 		alert('ESTE CODIGO NO EXISTE')";
	echo "	}\n";
	
	echo"
	}
	</script>	
	\n";
		
};
/**
 * 
 * @todo Funcion que genera una funcion en javascript para listar los depositos en el modulo de ajuste de inventario
 * @author Jean Carlos Nuñez
 * @return string
 *  
 */
function buscar_deposito_carga_descarga()
{
	global $conn; 
	$rs = mysqli_query($conn,"select * from depositos where estado = 1");

	echo "<script>\n";
	echo "function buscar_deposito()\n";
	echo "{\n";
	echo "var doc = document.form1; \n";
	echo "var bandera = 0; \n";
	echo "var var_cod_dep = doc.cod_dep.value; \n";
	echo "	if(var_cod_dep!=''){\n";while ($row_rs = mysqli_fetch_array($rs)){$var_codigo=$row_rs['codigo'];	$var_descripcion=$row_rs['descripcion'];
	echo "	if(var_cod_dep=='".$var_codigo."')\n";
	echo "	{ \n";
	echo "		doc.nom_dep.value='".$var_descripcion."'; \n";	
	echo "		bandera=1; \n";	
	echo "	}\n"; }
	echo "	if(bandera==0)\n";
	echo "	{ \n";
	echo " 		doc.cod_dep.value=''; doc.cod_dep.focus(); doc.nom_dep.value='';\n";	
	echo " 		alert('ESTE CODIGO NO EXISTE')";
	echo "	}}\n";
	
	echo"
	}
	</script>	
	\n";
		
};
/**
 * 
 * @todo Funcion que genera una funcion en javascript para listar los depositos
 * @author Jean Carlos Nuñez
 * @return string
 *  
 */
function buscar_deposito()
{
	global $conn; 
	$rs = mysqli_query($conn,"select * from depositos where estado = 1");

	echo "<script>\n";
	echo "function buscar_deposito()\n";
	echo "{\n";
	echo "var doc = document.form1; \n";	
	echo "var bandera = 0; \n"; 
	echo "var var_cod_dep = doc.cod_dep.value; \n";while ($row_rs = mysqli_fetch_array($rs)){$var_codigo=$row_rs['codigo']; 
	$var_descripcion=$row_rs['descripcion'];
	echo "	if(var_cod_dep=='".$var_codigo."')\n";
	echo "	{ \n";
	echo "		doc.nom_dep.value='".$var_descripcion."'; \n";		
	echo "		bandera=1; \n";
	echo "	}\n"; }
	echo "	if(bandera==0)\n";
	echo "	{ \n";
	echo " 		doc.cod_dep.value=''; doc.cod_dep.focus(); doc.nom_dep.value='';\n";	
	echo " 		alert('ESTE CODIGO NO EXISTE')";
	echo "	}\n";
	
	echo"
	}
	</script>	
	\n";
		
};
/**
 * 
 * @todo Funcion que genera una funcion en javascript para listar los proveedores
 * @author Jean Carlos Nuñez
 * @return string
 *  
 */
function buscar_proveedor()
{
	global $conn; 
	$rs = mysqli_query($conn,"select * from proveedores where estado = 1");
	echo "<script>\n";
	echo "function buscar_proveedor()\n";
	echo "{\n";
	echo "var doc = document.form1; \n";		
	echo "var bandera = 0; \n"; 
	echo "var var_cod_provee = doc.cod_provee.value; \n";
	echo "	if(var_cod_provee!='')\n";
	echo "	{ \n";while ($row_rs = mysqli_fetch_array($rs)){$var_codigo=$row_rs['codigo']; $var_descripcion=$row_rs['nombre'];
	echo "		if(var_cod_provee=='".$var_codigo."')\n";
	echo "		{ \n";
	echo "			doc.nom_provee.value='".$var_descripcion."'; \n";
	echo "			doc.cod_provee.value='".$var_codigo."'; \n";	
	echo "			bandera=1; \n";
	echo "		}\n"; }
	echo "		if(bandera==0)\n";
	echo "		{ \n";
	echo " 			doc.cod_provee.value=''; doc.cod_provee.focus(); doc.nom_provee.value='';\n";		
	echo " 			alert('ESTE CODIGO NO EXISTE')";
	echo "		}\n";
	echo "	}\n";
	echo"
	}	
	</script>	
	\n";
		
};

/**
 * 
 * @todo Funcion que genera una funcion en javascript para listar los autos
 * @author Jean Carlos Nuñez
 * @return string
 *  
 */
function buscar_numero_ope()
{
	global $conn; 
	$rs = mysqli_query($conn,"select * from autos order by num_und");

	echo "<script>\n";
	echo "function buscar_ope()\n";
	echo "{\n";
	echo "var doc = document.form1; \n"; 
	echo "var var_num_und = doc.num_und.value; \n";while ($row_rs = mysqli_fetch_array($rs)){$var_num_und=$row_rs['num_und'];$var_cod_emp=$row_rs['cod_emp'];$var_num_ope=$row_rs['num_ope'];
	echo "	if(var_num_und=='".$var_num_und."')\n";
	echo "	{ \n";
	echo "		doc.num_ope.value=".$var_num_ope."; \n";
	echo "		doc.cod_emp_ori.value=".$var_cod_emp."; \n";
	echo "	}\n"; }
	echo"
	}
	</script>	
	\n";
		
};

/**
 * 
 * @todo Funcion que busca todo los facturado por usuario
 * @author Jean Carlos Nuñez
 * @param int $var_cod_usu
 * @param date $var_fecha
 * @return decimal
 *  
 */
function facturacion_usuario_monto_actual($var_cod_usu_cajero,$var_fecha)
{

global $conn; 
	
	
	$sSql="select codigo from usuarios where estado = 1 and codigo = ".$var_cod_usu_cajero."";
	$rs_usu = mysqli_query($conn,$sSql);
	while ($row_rs_usu = mysqli_fetch_array($rs_usu))
	{		
		$var_cod_usu = $row_rs_usu['codigo'];
		$sSql="select sum(monto_dia) as monto_pagado
		from tickets where fecha_impresion = '".$var_fecha."' and cod_usu = ".$var_cod_usu." and tipo_pago = 1";
		$rs_tickets = mysqli_query($conn,$sSql);
		while ($rs_tickets_data = mysqli_fetch_array($rs_tickets))
		{$tickets=$rs_tickets_data['monto_pagado'];}
		if($tickets==""){$tickets=0;}

		$sSql="select sum(monto) as monto_pagado
		from siniestros_hist where fecha = '".$var_fecha."' and cod_usu = ".$var_cod_usu." and tipo_pago = 1";
		$rs_siniestros = mysqli_query($conn,$sSql);
		while ($rs_siniestros_data = mysqli_fetch_array($rs_siniestros))
		{$siniestro=$rs_siniestros_data['monto_pagado'];}
		if($siniestro==0){$siniestro=0;}

		$sSql="select sum(monto) as monto_pagado
		from multas_hist where fecha = '".$var_fecha."' and cod_usu = ".$var_cod_usu." and tipo_pago = 1";
		$rs_multas = mysqli_query($conn,$sSql);
		while ($rs_multas_data = mysqli_fetch_array($rs_multas))
		{$multas = $rs_multas_data['monto_pagado'];}
		if($multas==""){$multas=0;}

		$sSql="select sum(monto) as monto_pagado
		from inscripciones_hist where fecha = '".$var_fecha."' and cod_usu = ".$var_cod_usu." and tipo_pago = 1";
		$rs_inscripciones = mysqli_query($conn,$sSql);
		while ($rs_inscripciones_data = mysqli_fetch_array($rs_inscripciones))
		{$inscripciones = $rs_inscripciones_data['monto_pagado'];}
		if($inscripciones==""){$inscripciones=0;}

		$sSql="select sum(monto) as monto_pagado
		from ahorros_hist where fecha = '".$var_fecha."' and cod_usu = ".$var_cod_usu." and tipo_pago = 1";
		$rs_ahorro = mysqli_query($conn,$sSql);
		while ($rs_ahorro_data = mysqli_fetch_array($rs_ahorro))
		{$ahorro = $rs_ahorro_data['monto_pagado'];}
		if($ahorro==""){$ahorro=0;}
		
		$sSql="select sum(monto) as monto_pagado
		from ahorro_mantenimiento_hist where fecha = '".$var_fecha."' and cod_usu = ".$var_cod_usu." and tipo_pago = 1";
		$rs_ahorro_man = mysqli_query($conn,$sSql);
		while ($rs_ahorro_man_data = mysqli_fetch_array($rs_ahorro_man))
		{$ahorro_man = $rs_ahorro_man_data['monto_pagado'];}
		if($ahorro_man==""){$ahorro_man=0;}

		$sSql="select sum(monto) as monto_pagado
		from ahorro_dias_feriados_hist where fecha = '".$var_fecha."' and cod_usu = ".$var_cod_usu." and tipo_pago = 1";
		$rs_ahorro_feriados = mysqli_query($conn,$sSql);
		while ($rs_ahorro_feriados_data = mysqli_fetch_array($rs_ahorro_feriados))
		{$ahorro_feri = $rs_ahorro_feriados_data['monto_pagado'];}
		if($ahorro_feri==""){$ahorro_feri=0;}

		$sSql="select sum(monto_facturado) as monto_pagado
	    from cuadre_caja where fecha = '".$var_fecha."' and cod_usu_cajero = ".$var_cod_usu."";
	    $rs_cuadre_caja = mysqli_query($conn,$sSql);
	    while ($rs_cuadre_caja_data = mysqli_fetch_array($rs_cuadre_caja))
	    {$diferencia = $rs_cuadre_caja_data['monto_pagado'];}    
		if($diferencia==""){$diferencia=0;}
	    
	    $sSql="select sum(diferencia) as monto_pagado
	    from cuadre_caja where fecha = '".$var_fecha."' and cod_usu_cajero = ".$var_cod_usu."";
	    $rs_cuadre_caja = mysqli_query($conn,$sSql);
	    while ($rs_cuadre_caja_data = mysqli_fetch_array($rs_cuadre_caja))
	    {$diferencia_real = $rs_cuadre_caja_data['monto_pagado'];}    
		if($diferencia_real==""){$diferencia_real=0;}
	        
	    $diferencia = $diferencia_real + $diferencia;
	          
	    $total_facturado = ($tickets+$siniestro+$multas+$inscripciones+$ahorro+$ahorro_feri+$ahorro_man)-($diferencia);
	}
	return $total_facturado;
};
/**
 * 
 * @todo Funcion la factiracion actual del usuario
 * @author Jean Carlos Nuñez
 * @param date $var_fecha
 * @return decimal
 *  
 */
function facturacion_usuario_actual($var_fecha)
{

global $conn; 
	
	
	echo "<script>\n";
	echo "function facturacion_usuario_actual(cod_usu)\n";
	echo "{\n";
	echo "var doc = document.form1; \n";

	$sSql="select codigo from usuarios where estado = 1";
	$rs_usu = mysqli_query($conn,$sSql);
	while ($row_rs_usu = mysqli_fetch_array($rs_usu))
	{		
		
		$var_cod_usu = $row_rs_usu['codigo'];

		$sSql="select sum(monto_dia) as monto_pagado
		from tickets where fecha_impresion = '".$var_fecha."' and cod_usu = ".$var_cod_usu." and tipo_pago =1";
		$rs_tickets = mysqli_query($conn,$sSql);
		while ($rs_tickets_data = mysqli_fetch_array($rs_tickets))
		{$tickets=$rs_tickets_data['monto_pagado'];}
		if($tickets==""){$tickets=0;}
		
		$sSql="select sum(monto) as monto_pagado
		from siniestros_hist where fecha = '".$var_fecha."' and cod_usu = ".$var_cod_usu." and tipo_pago = 1";
		$rs_siniestros = mysqli_query($conn,$sSql);
		while ($rs_siniestros_data = mysqli_fetch_array($rs_siniestros))
		{$siniestro=$rs_siniestros_data['monto_pagado'];}
		if($siniestro==""){$siniestro=0;}

		$sSql="select sum(monto) as monto_pagado
		from multas_hist where fecha = '".$var_fecha."' and cod_usu = ".$var_cod_usu." and tipo_pago = 1";
		$rs_multas = mysqli_query($conn,$sSql);
		while ($rs_multas_data = mysqli_fetch_array($rs_multas))
		{$multas = $rs_multas_data['monto_pagado'];}
		if($multas==""){$multas=0;}

		$sSql="select sum(monto) as monto_pagado
		from inscripciones_hist where fecha = '".$var_fecha."' and cod_usu = ".$var_cod_usu." and tipo_pago = 1";
		$rs_inscripciones = mysqli_query($conn,$sSql);
		while ($rs_inscripciones_data = mysqli_fetch_array($rs_inscripciones))
		{$inscripciones = $rs_inscripciones_data['monto_pagado'];}
		if($inscripciones==""){$inscripciones=0;}

		$sSql="select sum(monto) as monto_pagado
		from ahorros_hist where fecha = '".$var_fecha."' and cod_usu = ".$var_cod_usu." and tipo_pago = 1";
		$rs_ahorro = mysqli_query($conn,$sSql);
		while ($rs_ahorro_data = mysqli_fetch_array($rs_ahorro))
		{$ahorro = $rs_ahorro_data['monto_pagado'];}
		if($ahorro==""){$ahorro=0;}
		
		
		
		$sSql="select sum(monto) as monto_pagado
		from ahorro_mantenimiento_hist where fecha = '".$var_fecha."' and cod_usu = ".$var_cod_usu." and tipo_pago = 1";
		$rs_ahorro_man = mysqli_query($conn,$sSql);
		while ($rs_ahorro_man_data = mysqli_fetch_array($rs_ahorro_man))
		{$ahorro_man = $rs_ahorro_man_data['monto_pagado'];}
		if($ahorro_man==""){$ahorro_man=0;}

		$sSql="select sum(monto) as monto_pagado
		from ahorro_dias_feriados_hist where fecha = '".$var_fecha."' and cod_usu = ".$var_cod_usu." and tipo_pago = 1";
		$rs_ahorro_feriados = mysqli_query($conn,$sSql);
		while ($rs_ahorro_feriados_data = mysqli_fetch_array($rs_ahorro_feriados))
		{$ahorro_feri = $rs_ahorro_feriados_data['monto_pagado'];}
		if($ahorro_feri==""){$ahorro_feri=0;}

		$sSql="select sum(monto_facturado) as monto_pagado
	    from cuadre_caja where fecha = '".$var_fecha."' and cod_usu_cajero = ".$var_cod_usu."";
	    $rs_cuadre_caja = mysqli_query($conn,$sSql);
	    while ($rs_cuadre_caja_data = mysqli_fetch_array($rs_cuadre_caja))
	    {$diferencia = $rs_cuadre_caja_data['monto_pagado'];}    
		if($diferencia==""){$diferencia=0;}

		$sSql="select sum(diferencia) as monto_pagado
	    from cuadre_caja where fecha = '".$var_fecha."' and cod_usu_cajero = ".$var_cod_usu."";
	    $rs_cuadre_caja = mysqli_query($conn,$sSql);
	    while ($rs_cuadre_caja_data = mysqli_fetch_array($rs_cuadre_caja))
	    {$diferencia_real = $rs_cuadre_caja_data['monto_pagado'];}    
		if($diferencia_real==""){$diferencia_real=0;}
	        
	    $diferencia = $diferencia_real + $diferencia;

	    $total_facturado = (($tickets)+($siniestro)+($multas)+($inscripciones)+($ahorro)+($ahorro_feri)+($ahorro_man))-($diferencia);	    

	    

	    if($row_rs_usu['codigo']<>'')
	    {
			echo "	if(cod_usu=='".$row_rs_usu['codigo']."')\n";
			echo "	{ \n";
			echo " 		doc.monto_facturado.value=".$total_facturado.";\n";
			echo "      siguiente(".$total_facturado.");\n";
			echo "	}\n"; 
			
		}
		
	}
	echo" 
	}
	</script>	
	\n";
		
};
/**
 * 
 * @todo Funcion facturaacion por usuario por fecha
 * @author Jean Carlos Nuñez
 * @param date $var_fecha
 * @return decimal
 *  
 */
function facturacion_usuario_fecha($var_fecha)
{

global $conn; 
	
	
	echo "<script>\n";
	echo "function facturacion_usuario_actual(cod_usu)\n";
	echo "{\n";
	echo "var doc = document.form1; \n";

	$sSql="select codigo from usuarios where estado = 1";
	$rs_usu = mysqli_query($conn,$sSql);
	while ($row_rs_usu = mysqli_fetch_array($rs_usu))
	{		
		
		$var_cod_usu = $row_rs_usu['codigo'];

		$sSql="select sum(monto_dia) as monto_pagado
		from tickets where fecha_impresion = '".$var_fecha."' and cod_usu = ".$var_cod_usu." and tipo_pago = 1";
		//echo $sSql;
		$rs_tickets = mysqli_query($conn,$sSql);
		while ($rs_tickets_data = mysqli_fetch_array($rs_tickets))
		{$tickets=$rs_tickets_data['monto_pagado'];}
		if($tickets==""){$tickets=0;}
		//echo $tickets;
		$sSql="select sum(monto) as monto_pagado
		from siniestros_hist where fecha = '".$var_fecha."' and cod_usu = ".$var_cod_usu." and tipo_pago = 1";
		$rs_siniestros = mysqli_query($conn,$sSql);
		while ($rs_siniestros_data = mysqli_fetch_array($rs_siniestros))
		{$siniestro=$rs_siniestros_data['monto_pagado'];}
		if($siniestro==""){$siniestro=0;}

		$sSql="select sum(monto) as monto_pagado
		from multas_hist where fecha = '".$var_fecha."' and cod_usu = ".$var_cod_usu." and tipo_pago = 1";
		$rs_multas = mysqli_query($conn,$sSql);
		while ($rs_multas_data = mysqli_fetch_array($rs_multas))
		{$multas = $rs_multas_data['monto_pagado'];}
		if($multas==""){$multas=0;}

		$sSql="select sum(monto) as monto_pagado
		from inscripciones_hist where fecha = '".$var_fecha."' and cod_usu = ".$var_cod_usu." and tipo_pago = 1";
		$rs_inscripciones = mysqli_query($conn,$sSql);
		while ($rs_inscripciones_data = mysqli_fetch_array($rs_inscripciones))
		{$inscripciones = $rs_inscripciones_data['monto_pagado'];}
		if($inscripciones==""){$inscripciones=0;}

		$sSql="select sum(monto) as monto_pagado
		from ahorros_hist where fecha = '".$var_fecha."' and cod_usu = ".$var_cod_usu." and tipo_pago = 1";
		$rs_ahorro = mysqli_query($conn,$sSql);
		while ($rs_ahorro_data = mysqli_fetch_array($rs_ahorro))
		{$ahorro = $rs_ahorro_data['monto_pagado'];}
		if($ahorro==""){$ahorro=0;}
		
		$sSql="select sum(monto) as monto_pagado
		from ahorro_mantenimiento_hist where fecha = '".$var_fecha."' and cod_usu = ".$var_cod_usu." and tipo_pago = 1";
		$rs_ahorro_man = mysqli_query($conn,$sSql);
		while ($rs_ahorro_man_data = mysqli_fetch_array($rs_ahorro_man))
		{$ahorro_man = $rs_ahorro_man_data['monto_pagado'];}
		if($ahorro_man==""){$ahorro_man=0;}

		$sSql="select sum(monto) as monto_pagado
		from ahorro_dias_feriados_hist where fecha = '".$var_fecha."' and cod_usu = ".$var_cod_usu." and tipo_pago = 1";
		$rs_ahorro_feriados = mysqli_query($conn,$sSql);
		while ($rs_ahorro_feriados_data = mysqli_fetch_array($rs_ahorro_feriados))
		{$ahorro_feri = $rs_ahorro_feriados_data['monto_pagado'];}
		if($ahorro_feri==""){$ahorro_feri=0;}

		$sSql="select sum(monto_facturado) as monto_pagado
	    from cuadre_caja where fecha = '".$var_fecha."' and cod_usu_cajero = ".$var_cod_usu." ";
	    $rs_cuadre_caja = mysqli_query($conn,$sSql);
	    while ($rs_cuadre_caja_data = mysqli_fetch_array($rs_cuadre_caja))
	    {$diferencia = $rs_cuadre_caja_data['monto_pagado'];}    
		if($diferencia==""){$diferencia=0;}
	    $total_facturado=0;    

	    $sSql="select sum(diferencia) as monto_pagado
	    from cuadre_caja where fecha = '".$var_fecha."' and cod_usu_cajero = ".$var_cod_usu."";
	    $rs_cuadre_caja = mysqli_query($conn,$sSql);
	    while ($rs_cuadre_caja_data = mysqli_fetch_array($rs_cuadre_caja))
	    {$diferencia_real = $rs_cuadre_caja_data['monto_pagado'];}    
		if($diferencia_real==""){$diferencia_real=0;}	 

	    $diferencia = $diferencia_real + $diferencia;

	    $total_facturado = (($tickets)+($siniestro)+($multas)+($inscripciones)+($ahorro)+($ahorro_feri)+($ahorro_man))-($diferencia);	    

	    

	    if($row_rs_usu['codigo']<>'')
	    {
			echo "	if(cod_usu=='".$row_rs_usu['codigo']."')\n";
			echo "	{ \n";
			echo " 		doc.monto_facturado.value=".$total_facturado.";\n";
			echo "      siguiente(".$total_facturado.");\n";
			echo "	}\n"; 
			
		}
		
	}
	echo" 
	}
	</script>	
	\n";
		
};
/**
 * 
 * @todo Funcion que da el monto facturado por usuario
 * @author Jean Carlos Nuñez
 * @return decimal
 *  
 */
function facturacion_usuario()
{

global $conn; 
	
	
	echo "<script>\n";
	echo "function facturacion_usuario(cod_usu)\n";
	echo "{\n";
	echo "var doc = document.form1; \n";

	$sSql="select codigo from usuarios where estado = 1";
	$rs_usu = mysqli_query($conn,$sSql);
	while ($row_rs_usu = mysqli_fetch_array($rs_usu))
	{		
		$var_cod_usu = $row_rs_usu['codigo'];
		$sSql="select sum(monto_dia) as monto_pagado
		from tickets where fecha_impresion = '".fecha_aplicacion($conn)."' and cod_usu = ".$var_cod_usu." and tipo_pago = 1";
		$rs_tickets = mysqli_query($conn,$sSql);
		while ($rs_tickets_data = mysqli_fetch_array($rs_tickets))
		{$tickets=$rs_tickets_data['monto_pagado'];}
		
		$sSql="select sum(monto) as monto_pagado
		from siniestros_hist where fecha = '".fecha_aplicacion($conn)."' and cod_usu = ".$var_cod_usu." and tipo_pago = 1";
		$rs_siniestros = mysqli_query($conn,$sSql);
		while ($rs_siniestros_data = mysqli_fetch_array($rs_siniestros))
		{$siniestro=$rs_siniestros_data['monto_pagado'];}

		$sSql="select sum(monto) as monto_pagado
		from multas_hist where fecha = '".fecha_aplicacion($conn)."' and cod_usu = ".$var_cod_usu." and tipo_pago = 1";
		$rs_multas = mysqli_query($conn,$sSql);
		while ($rs_multas_data = mysqli_fetch_array($rs_multas))
		{$multas = $rs_multas_data['monto_pagado'];}

		$sSql="select sum(monto) as monto_pagado
		from inscripciones_hist where fecha = '".fecha_aplicacion($conn)."' and cod_usu = ".$var_cod_usu." and tipo_pago = 1";
		$rs_inscripciones = mysqli_query($conn,$sSql);
		while ($rs_inscripciones_data = mysqli_fetch_array($rs_inscripciones))
		{$inscripciones = $rs_inscripciones_data['monto_pagado'];}

		$sSql="select sum(monto) as monto_pagado
		from ahorros_hist where fecha = '".fecha_aplicacion($conn)."' and cod_usu = ".$var_cod_usu." and tipo_pago = 1";
		$rs_ahorro = mysqli_query($conn,$sSql);
		while ($rs_ahorro_data = mysqli_fetch_array($rs_ahorro))
		{$ahorro = $rs_ahorro_data['monto_pagado'];}
		
		$sSql="select sum(monto) as monto_pagado
		from ahorro_mantenimiento_hist where fecha = '".fecha_aplicacion($conn)."' and cod_usu = ".$var_cod_usu." and tipo_pago = 1";
		$rs_ahorro_man = mysqli_query($conn,$sSql);
		while ($rs_ahorro_man_data = mysqli_fetch_array($rs_ahorro_man))
		{$ahorro_man = $rs_ahorro_man_data['monto_pagado'];}

		$sSql="select sum(monto) as monto_pagado
		from ahorro_dias_feriados_hist where fecha = '".fecha_aplicacion($conn)."' and cod_usu = ".$var_cod_usu." and tipo_pago = 1";
		$rs_ahorro_feriados = mysqli_query($conn,$sSql);
		while ($rs_ahorro_feriados_data = mysqli_fetch_array($rs_ahorro_feriados))
		{$ahorro_feri = $rs_ahorro_feriados_data['monto_pagado'];}


		
		$total_facturado=0;		
		$total_facturado = $tickets+$siniestro+$multas+$inscripciones+$ahorro+$ahorro_feri+$ahorro_man;

		

		if($total_facturado>0)
		{
	
			echo "	if(cod_usu=='".$row_rs_usu['codigo']."')\n";
			echo "	{ \n";
			echo " 		doc.monto_facturado.value=".$total_facturado."";		
			echo "	}\n"; 

			echo "	if(cod_usu=='')\n";
			echo "	{ \n";
			echo " 		doc.monto_facturado.value=''";		
			echo "	}\n"; 
		}
	}
	echo" 
	}
	</script>	
	\n";
		
};
/**
 * 
 * @todo Funcion que genera una funcion en javascript por operador por cedula
 * @author Jean Carlos Nuñez
 * @return string
 *  
 */
function buscar_operador_cedula_rt()
{
	global $conn; 
	$rs = mysqli_query($conn,"select * from rt_operadores order by num_oper");

	echo "<script>\n";
	echo "function buscar_operador_cedula_rt()\n";
	echo "{\n";
	echo "var doc = document.form1; \n";	
	echo "var bandera = 0; \n"; 
	echo "var var_cedula = doc.cedula.value; \n";while ($row_rs = mysqli_fetch_array($rs))
	{
		$var_num_oper=trim($row_rs['num_oper']);$var_cedula=trim($row_rs['cedula']);		
	echo "	if(var_cedula=='".$var_cedula."')\n";
	echo "	{ \n";
	echo " 		alert('ESTE NUMERO DE CEDULA YA EXISTE');doc.cedula.value='';doc.cedula.focus();return false;";		
	echo "	}\n"; }
	echo"
	}
	</script>	
	\n";
		
};
/**
 * 
 * @todo Funcion que genera una funcion en javascript por operador por cedula
 * @author Jean Carlos Nuñez
 * @return string
 *  
 */
function buscar_operador_cedula()
{
	global $conn; 
	$rs = mysqli_query($conn,"select * from operadores order by num_oper");

	echo "<script>\n";
	echo "function buscar_operador_cedula()\n";
	echo "{\n";
	echo "var doc = document.form1; \n";	
	echo "var bandera = 0; \n"; 
	echo "var var_cedula = doc.cedula.value; \n";while ($row_rs = mysqli_fetch_array($rs))
	{
		$var_num_oper=trim($row_rs['num_oper']);$var_cedula=trim($row_rs['cedula']);
		$var_nombre=trim($row_rs['nombre']." ".$row_rs['apellido']);
	echo "	if(var_cedula=='".$var_cedula."')\n";
	echo "	{ \n";
	echo "		doc.num_ope.value='".$var_num_oper."'; \n";
	echo "		doc.nombre_ope.value='".$var_nombre."'; \n";
	echo "		doc.cedula.focus(); \n";
	echo "		bandera=1; \n";		
	echo "	}\n"; }
	echo "	if(bandera==0)\n";
	echo "	{ \n";
	echo " 		doc.num_ope.value=''; doc.cedula.focus(); doc.nombre_ope.value=''; \n";
	echo " 		alert('ESTE NUMERO DE CEDULA NO EXISTE')";
	echo "	}\n";	
	echo"
	}
	</script>	
	\n";
		
};

/**
 * 
 * @todo Funcion que genera una funcion en javascript para buscar autos
 * @author Jean Carlos Nuñez
 * @return string
 *  
 */
function buscar_auto_salidas()
{
	global $conn; 
	$rs = mysqli_query($conn,"select * from autos order by num_und");

	echo "<script>\n";
	echo "function buscar_auto_salidas()\n";
	echo "{\n";
	echo "var doc = document.form1; \n";	
	echo "var bandera = 0; \n"; 
	echo "var var_num_und = doc.num_und.value; \n";while ($row_rs = mysqli_fetch_array($rs))
	{
		$var_num_und=trim($row_rs['num_und']);$var_kilometraje=trim($row_rs['kilometraje']);			
	echo "	if(var_num_und=='".$var_num_und."')\n";
	echo "	{ \n";
	echo "		doc.kilometraje.value='".$var_kilometraje."'; \n";	
	echo "		doc.codigo_sec.focus(); \n";
	echo "		bandera=1; return false;   \n";		
	echo "	}\n"; }
	echo "	if(bandera==0)\n";
	echo "	{ \n";
	echo " 		 //doc.kilometraje.focus(); \n";
	echo " 		 doc.kilometraje.value=''; \n";				
	echo " 		alert('ESTE NUMERO DE UNIDAD NO EXISTE'); return false;";
	echo "	}\n";	
	echo"
	}
	</script>	
	\n";
		
};
/**
 * 
 * @todo Funcion que genera una funcion en javascript para buscar autos
 * @author Jean Carlos Nuñez
 * @return string
 *  
 */
function buscar_auto()
{
	global $conn; 
	$rs = mysqli_query($conn,"select * from autos order by num_und");

	echo "<script>\n";
	echo "function buscar_auto()\n";
	echo "{\n";
	echo "var doc = document.form1; \n";	
	echo "var bandera = 0; \n"; 
	echo "var var_num_und = doc.num_und.value; \n";while ($row_rs = mysqli_fetch_array($rs))
	{
		$var_placa=trim($row_rs['placa']);$var_num_und=trim($row_rs['num_und']);		
	echo "	if(var_num_und=='".$var_num_und."')\n";
	echo "	{ \n";
	echo "		doc.placa.value='".$var_placa."'; \n";	
	echo "		doc.num_und.focus(); \n";
	echo "		bandera=1; \n";		
	echo "	}\n"; }
	echo "	if(bandera==0)\n";
	echo "	{ \n";
	echo " 		 doc.num_und.focus(); doc.placa.value=''; \n";
	echo " 		alert('ESTE NUMERO DE UNIDAD NO EXISTE')";
	echo "	}\n";	
	echo"
	}
	</script>	
	\n";
		
};
/**
 * 
 * @todo Funcion que genera una funcion en javascript para buscar operadores
 * @author Jean Carlos Nuñez
 * @return string
 *  
 */
function buscar_operador()
{
	global $conn; 
	$rs = mysqli_query($conn,"select * from operadores order by num_oper");

	echo "<script>\n";
	echo "function buscar_operador()\n";
	echo "{\n";
	echo "var doc = document.form1; \n";	
	echo "var bandera = 0; \n"; 
	echo "var var_num_ope = doc.num_ope.value; \n";while ($row_rs = mysqli_fetch_array($rs))
	{
		$var_num_oper=trim($row_rs['num_oper']);$var_cedula=trim($row_rs['cedula']);
		$var_nombre=trim($row_rs['nombre']." ".$row_rs['apellido']);
	echo "	if(var_num_ope=='".$var_num_oper."')\n";
	echo "	{ \n";
	echo "		doc.cedula.value='".$var_cedula."'; \n";
	echo "		doc.nombre_ope.value='".$var_nombre."'; \n";
	echo "		doc.num_ope.focus(); \n";
	echo "		bandera=1; \n";		
	echo "	}\n"; }
	echo "	if(bandera==0)\n";
	echo "	{ \n";
	echo " 		 doc.num_ope.focus(); doc.nombre_ope.value=''; doc.cedula.value='';\n";
	echo " 		alert('ESTE NUMERO DE OPERADOR NO EXISTE')";
	echo "	}\n";	
	echo"
	}
	</script>	
	\n";
		
};
/**
 * 
 * @todo Funcion que genera una funcion en javascript para buscar autos
 * @author Jean Carlos Nuñez
 * @return string
 *  
 */
function buscar_numero_auto()
{
	global $conn; 
	$rs = mysqli_query($conn,"select * from autos order by num_und");

	echo "<script>\n";
	echo "function buscar_auto()\n";
	echo "{\n";
	echo "var doc = document.form1; \n"; 
	echo "var var_num_auto = doc.num_auto.value; \n";while ($row_rs = mysqli_fetch_array($rs)){$var_num_und=$row_rs['num_und'];
	echo "	if(var_num_auto=='".$var_num_und."')\n";
	echo "	{ \n";
	echo "		alert('ESTE NUMERO DE UNIDAD YA EXISTE EN LA BASE DE DATOS:$var_num_und');doc.num_auto.focus(); doc.num_auto.value=''; \n";
	echo "	}\n"; }
	echo"
	}
	</script>	
	\n";
		
};
/**
 * 
 * @todo Funcion que genera una funcion en javascript para buscar autos con sus diarios
 * @author Jean Carlos Nuñez
 * @return string
 *  
 */
function buscar_auto_diarios()
{
	global $conn; 
	$var_hora = date("H:i:s");
	
	$rs=phpmkr_query("select * from operaciones_generales ",$conn) 
	or die("Fallo al ejecutar la consulta en la l?nea" . __LINE__ . ": " . phpmkr_error($conn) . '<br>SQL: ' . $sSql);
	while ($row_rs = $rs->fetch_assoc())
	{
		$var_hora_termino_dia=$row_rs['hora_termino_dia'];				
	}
	
	$var_cantidad_horas=RestarHoras($var_hora,$var_hora_termino_dia);

	$rs = mysqli_query($conn,"select a.monto_diario,a.num_und from autos a where a.num_ope = 0");
	echo "<script>\n";
	echo "function buscar_auto_diarios()\n";
	echo "{\n";
	echo "var doc = document.form1; \n"; 
	echo "var horas_calcular =".$var_cantidad_horas.";\n";
	echo "var var_num_auto = doc.num_und.value; \n";while ($row_rs = mysqli_fetch_array($rs))
	{
		$var_num_und=$row_rs['num_und']; $var_monto_diario_por_hora=number_format($row_rs['monto_diario']/24,2);
	echo "	if(var_num_auto=='".$var_num_und."')\n";	
	echo "	{ \n";
	echo "      var var_monto_cobrar = parseInt(horas_calcular) * parseFloat(".$var_monto_diario_por_hora.")\n";
	echo "		doc.diario.value=var_monto_cobrar;";
	echo "	}\n"; }
	echo"
	}
	</script>	
	\n";		
};

/**
 * 
 * @todo Funcion que genera una funcion en javascript para buscar las fecha de las facturacion para el cierre del sistema
 * @author Jean Carlos Nuñez
 * @return string
 *  
 */
function buscar_fecha_diarios()
{
	global $conn; 
	$rs = mysqli_query($conn,"select distinct(fecha_impresion) as fecha from tickets order by fecha_impresion desc limit 30");

	echo "<script>\n";
	echo "function buscar_fecha_diarios()\n";
	echo "{\n";
	echo "var doc = document.form1; \n"; while ($row_rs = mysqli_fetch_array($rs)){$var_fecha=$row_rs['fecha'];
	echo "var var_fecha_actual = doc.fecha_actual.value; \n";
	echo "	if(var_fecha_actual=='".$var_fecha."')\n";
	echo "	{ \n";
	echo "		alert('NO SE PUEDE HACER LA REVERSION CON ESTA FECHA: ".$var_fecha."');doc.revertir.value=0; \n";
	echo "	}\n"; }
	echo"
	}
	</script>	
	\n";
		
};

/**
 * 
 * @todo Funcion que genera una funcion en javascript para buscar fecha de cuentas por cobrar por usuario
 * @author Jean Carlos Nuñez
 * @param int $var_num_ope
 * @return string
 *  
 */
function buscar_fecha($var_num_ope)
{
	global $conn; 
	$rs = mysqli_query($conn,"select distinct(fecha) as fecha from cuentasxcobrar_hist where num_ope = $var_num_ope");
	echo "<script>\n";
	echo "function buscar_fecha()\n";
	echo "{\n";
	echo "var doc = document.form1; \n";
	echo "var var_monto_ins = doc.monto_ins.value; \n";
	echo "var valor = validar_insertar() \n";	
	echo "	if(var_monto_ins=='' )\n";
	echo "	{ \n";
	echo "		alert('SE REQUIERE LLENAR EL CAMPO DE MONTO DE DIARIO');doc.monto_ins.focus(); doc.insertar.disabled='false'; return false;  \n";
	echo "	}\n";	
	echo "  var var_fecha_ins = doc.fecha_ins.value; \n";while ($row_rs = mysqli_fetch_array($rs)){$var_fecha_actual=fecha($row_rs['fecha']);
	echo "	if(var_fecha_ins=='".$var_fecha_actual."')\n";
	echo "	{ \n";
	echo "		alert('YA EXISTE ESTA FECHA, NO PUEDE CREAR OTRO CON LA MISMA: ".$var_fecha_actual."'); return false;  \n";
	echo "	}\n"; }
	echo "	if(valor==false){doc.insertar.disabled='false'; return false;}else{doc.insertar.disabled='true';}\n";
	echo"
	}
	</script>	
	\n";
		
};

/**
 * 
 * @todo Funcion que genera una funcion en javascript para buscar facturas
 * @author Jean Carlos Nuñez
 * @return string
 *  
 */
function buscar_factura()
{
	global $conn; 
	$rs = mysqli_query($conn,"select distinct(factura) from entradas order by factura");
	echo "<script>\n";
	echo "function buscar_factura()\n";
	echo "{\n";
	echo "var doc = document.form1; \n"; 
	echo "var var_factura = doc.factura.value.toUpperCase();; \n";while ($row_rs = mysqli_fetch_array($rs)){$var_factura=$row_rs['factura'];
	echo "	if(var_factura=='".$var_factura."')\n";
	echo "	{ \n";
	echo "		alert('ESTA FACTURA YA EXISTE EN LA BASE DE DATOS:$var_factura');doc.factura.focus(); doc.factura.value=''; \n";
	echo "	}\n"; }
	echo"
	}
	</script>	
	\n";
		
};
/**
 * 
 * @todo Funcion que genera una funcion en javascript para buscar factura en ordenes de compra
 * @author Jean Carlos Nuñez
 * @return string
 *  
 */
function buscar_factura_odc()
{
	global $conn; 
	$rs = mysqli_query($conn,"select distinct(factura) from entradas order by factura");
	echo "<script>\n";
	echo "function buscar_factura_odc()\n";
	echo "{\n";
	echo "var doc = document.form1; \n"; 
	echo "var var_factura = doc.factura.value.toUpperCase();; \n";while ($row_rs = mysqli_fetch_array($rs)){$var_factura=$row_rs['factura'];
	echo "	if(var_factura=='".$var_factura."')\n";
	echo "	{ \n";
	echo "		alert('ESTA FACTURA YA EXISTE EN LA BASE DE DATOS:$var_factura');doc.factura.focus(); doc.factura.value=''; \n";
	echo "	}\n"; }
	echo"
	}
	</script>	
	\n";
		
};
/**
 * 
 * @todo Funcion que genera una funcion en javascript para buscar responsable por cedula
 * @author Jean Carlos Nuñez
 * @return string
 *  
 */
function buscar_cedula_responsables()
{
	global $conn; 
	$rs = mysqli_query($conn,"select * from responsables_salidas order by cedula");
	echo "<script>\n";
	echo "function buscar_cedula_responsables()\n";
	echo "{\n";
	echo "var doc = document.form1; \n"; 
	echo "var var_cedula = doc.cedula.value; \n";while ($row_rs = mysqli_fetch_array($rs)){$var_cedula=$row_rs['cedula'];$var_nombre=$row_rs['nombre'];
	echo "	if(var_cedula=='".$var_cedula."')\n";
	echo "	{ \n";
	echo "		doc.nombre.value ='".$var_nombre."'";
	echo "	}\n"; }
	echo"
	}
	</script>	
	\n";
		
};
/**
 * 
 * @todo Funcion que genera una funcion en javascript para buscar reponsable por cedula
 * @author Jean Carlos Nuñez
 * @return string
 *  
 */
function buscar_cedula()
{
	global $conn; 
	$rs = mysqli_query($conn,"select * from responsables_salidas order by cedula");
	echo "<script>\n";
	echo "function buscar_cedula()\n";
	echo "{\n";
	echo "var doc = document.form1; \n"; 
	echo "var var_cedula = doc.cedula.value; \n";while ($row_rs = mysqli_fetch_array($rs)){$var_cedula=$row_rs[cedula];
	echo "	if(var_cedula=='".$var_cedula."')\n";
	echo "	{ \n";
	echo "		alert('ESTA CEDULA YA EXISTE EN LA BASE DE DATOS:$var_cedula');doc.cedula.focus(); doc.cedula.value=''; \n";
	echo "	}\n"; }
	echo"
	}
	</script>	
	\n";
		
};
/**
 * 
 * @todo Funcion que genera una funcion en javascript para buscar validar las acciones en los modulos de la aplicacion
 * @author Jean Carlos Nuñez
 * @return string
 *  
 */

function validar_acciones()
{
	echo "<script>\n";
	echo "function validar_acciones(valor)\n";
	echo "{\n";
	echo "if(valor=='1'){alert('EL REGISTRO FUE INSERTADO CON EXITO!');} \n"; 	
	echo "if(valor=='2'){alert('EL REGISTRO FUE MODIFICADO CON EXITO!');} \n"; 
	echo "if(valor=='3'){alert('EL REGISTRO FUE ELIMINADO CON EXITO!');} \n"; 		
	echo "	}\n";
	echo "</script>\n";
	
};
/**
 * 
 * @todo Funcion que genera una funcion en javascript para alidar las acciones en los modulos en la taquilla
 * @author Jean Carlos Nuñez
 * @return string
 *  
 */
function validar_acciones_taquilla()
{
	echo "<script>\n";
	echo "function validar_acciones_taquilla(valor)\n";
	echo "{\n";
	echo "var doc = document.form1; \n"; 	
	echo "if(valor==99) {alert('ESTA UNIDAD NO EXISTE'); doc.num_und.focus();} \n"; 	
	echo "if(valor==100){alert('NO PUEDE DEJAR CAMPOS EN BLANCO'); doc.num_und.focus();} \n"; 		
	echo "if(valor==101){alert('ESTA UNIDAD NO TIENE OPERADOR ASIGNADO'); doc.num_und.focus();} \n"; 		
	echo "if(valor==1)  {doc.kilometraje.focus();} \n"; 		
	echo "if(valor=='') {doc.num_und.focus();} \n"; 		
	echo "	}\n";
	echo "</script>\n";
	
};

/**
 * 
 * @todo Funcion que genera una funcion en javascript para alidar las acciones en el modulo del cierre
 * @author Jean Carlos Nuñez
 * @return string
 *  
 */
function validar_acciones_cierre()
{
	echo "<script>\n";
	echo "function validar_acciones_cierre(valor)\n";
	echo "{\n";
	echo "if(valor=='1'){alert('EL CIERRE SE REALIZO SATISFACTORIAMENTE');} \n"; 	
	echo "if(valor=='2'){alert('EL CIERRE SE REVIRTIO SATISFACTORIAMENTE');} \n"; 		
	echo "	}\n";
	echo "</script>\n";
	
};
/**
 * 
 * @todo Funcion que genera una funcion en javascript para alidar las acciones en el login de la aplicacion
 * @author Jean Carlos Nuñez
 * @return string
 *  
 */
function validar_acciones_login()
{
	echo "<script>\n";
	echo "function validar_acciones_login(valor)\n";
	echo "{var doc = document.form1;\n";
	echo "if(valor==0){alert('Este Usuario no Existe en la Base de Datos o esta Desactivado');doc.usuario.focus();}else{doc.usuario.focus();} \n"; 	
	echo "	}\n";
	echo "</script>\n";
	
};
/**
 * 
 * @todo Funcion para concatenar string
 * @author Jean Carlos Nuñez
 * @param string $cadena
 * @param int $longitud_max
 * @return string
 *  
 */
function concatenacion($cadena,$longitud_max) 
{
	if(strlen($cadena) < $longitud_max) 

	{	

		$vowels = array(" ");

		$cadena= str_replace($vowels, "0",$cadena);

		$var_cantidad=strlen($cadena);

		$var_cantidad=$longitud_max-$var_cantidad;

		$cadena=str_repeat("0",$var_cantidad)."".$cadena;

		return $cadena;

	}

	else{return $cadena;} 

}
/**
 * 
 * @todo Funcion que registra todas las acciones en la atraves de la aplicacion OpenPhoneTaxi
 * @author Jean Carlos Nuñez
 * @param int $var_cod_usu
 * @param string $var_accion
 * @param db $conne
 * @return Null
 *  
 */
function auditoria_openphonetaxi($var_cod_usu,$var_accion,$conne) 
{
	$rs=phpmkr_query("select fecha from fecha_aplicacion ",$conne) 
	or die("Fallo al ejecutar la consulta en la linea" . __LINE__ . ": " . phpmkr_error($conne) . '<br>SQL: ' . $sSql);
	while ($row_rs = $rs->fetch_assoc())
	{$var_fecha_actual=$row_rs['fecha'];}

	$rs=phpmkr_query("select auditoria from operaciones_generales ",$conne) 
	or die("Fallo al ejecutar la consulta en la linea" . __LINE__ . ": " . phpmkr_error($conne) . '<br>SQL: ' . $sSql);
	while ($row_rs = $rs->fetch_assoc())
	{$var_auditoria=$row_rs['auditoria'];}

	$rs=phpmkr_query("select curtime() as hora_actual ",$conne) 
	or die("Fallo al ejecutar la consulta en la linea" . __LINE__ . ": " . phpmkr_error($conne) . '<br>SQL: ' . $sSql);
	while ($row_rs = $rs->fetch_assoc())
	{$hora=$row_rs['hora_actual'];}

	

	$rs=phpmkr_query("select fecha_hasta from operaciones_generales ",$conne) 
	or die("Fallo al ejecutar la consulta en la linea" . __LINE__ . ": " . phpmkr_error($conne) . '<br>SQL: ' . $sSql);
	while ($row_rs = $rs->fetch_assoc())
	{$var_fecha_hasta=$row_rs['fecha_hasta'];}

	if($var_fecha_actual>$var_fecha_hasta)
	{		
		header("Location: index.php");exit();	
	}

	if($var_auditoria==1)
	{
		$var_ip = $_SERVER['REMOTE_ADDR'];	

		
		
		$contador=1;

		//$hora = date('H:i:s');
		$today = date("Y-m-d");
		if($contador>0)
		{
			$sSql="insert into auditoria values($var_cod_usu,'$today','$hora','$var_accion','$var_ip') ";
			phpmkr_query($sSql,$conne);
			errores($sSql);
		}
		else
		{
			$sSql="insert into auditoria values($var_cod_usu,'$today','$hora','IP NO ENCONTRADA','$var_ip') ";
			phpmkr_query($sSql,$conne);
			//errores($sSql);		
		}
	}
}
/**
 * 
 * @todo Funcion que registra todas las acciones en la aplicacion
 * @author Jean Carlos Nuñez
 * @param int $var_cod_usu
 * @param string $var_accion
 * @param db $conne
 * @return Null
 *  
 */
function auditoria($var_cod_usu,$var_accion,$conne) 
{
	$rs=phpmkr_query("select fecha from fecha_aplicacion ",$conne) 
	or die("Fallo al ejecutar la consulta en la linea" . __LINE__ . ": " . phpmkr_error($conne) . '<br>SQL: ' . $sSql);
	while ($row_rs = $rs->fetch_assoc())
	{$var_fecha_actual=$row_rs['fecha'];}

	$rs=phpmkr_query("select auditoria from operaciones_generales ",$conne) 
	or die("Fallo al ejecutar la consulta en la linea" . __LINE__ . ": " . phpmkr_error($conne) . '<br>SQL: ' . $sSql);
	while ($row_rs = $rs->fetch_assoc())
	{$var_auditoria=$row_rs['auditoria'];}

	$rs=phpmkr_query("select curtime() as hora_actual ",$conne) 
	or die("Fallo al ejecutar la consulta en la linea" . __LINE__ . ": " . phpmkr_error($conne) . '<br>SQL: ' . $sSql);
	while ($row_rs = $rs->fetch_assoc())
	{$hora=$row_rs['hora_actual'];}

	

	$rs=phpmkr_query("select fecha_hasta from operaciones_generales ",$conne) 
	or die("Fallo al ejecutar la consulta en la linea" . __LINE__ . ": " . phpmkr_error($conne) . '<br>SQL: ' . $sSql);
	while ($row_rs = $rs->fetch_assoc())
	{$var_fecha_hasta=$row_rs['fecha_hasta'];}

	if($var_fecha_actual>$var_fecha_hasta)
	{		
		header("Location: index.php");exit();	
	}

	if($var_auditoria==1)
	{
		$var_ip = $_SERVER['REMOTE_ADDR'];	

		$contador = validar_ip_autorizada($var_ip);
		//$contador=1;
		//$hora = date('H:i:s');
		$today = date("Y-m-d");
		if($contador>0)
		{
			phpmkr_query("insert into auditoria values($var_cod_usu,'$today','$hora','$var_accion','$var_ip') ",$conne);
		}
		else
		{
			phpmkr_query("insert into auditoria values($var_cod_usu,'$today','$hora','IP NO ENCONTRADA','$var_ip') ",$conne);
			$_SESSION['accion']='3';
			header("Location: index.php");exit();		
		}
	}
}

/**
 * 
 * @todo Funcion que registra las acciones de los operadores
 * @author Jean Carlos Nuñez
 * @param int $var_num_ope
 * @param int $var_cod_usu
 * @param string $accion
 * @param db $conne
 * @return Null
 *  
 */


function acciones_operador($var_num_ope,$var_cod_usu,$var_accion,$conne) 
{
	$rs=phpmkr_query("select fecha from fecha_aplicacion ",$conne) 
	or die("Fallo al ejecutar la consulta en la l?nea" . __LINE__ . ": " . phpmkr_error($conne) . '<br>SQL: ' . $sSql);
	while ($row_rs = $rs->fetch_assoc())
	{$var_fecha_actual=$row_rs['fecha'];}	
	$hora = date("H:i:s");	
	phpmkr_query("insert into acciones_operador values($var_num_ope,'$var_accion','$var_fecha_actual','$hora',$var_cod_usu) ",$conne);

}
/**
 * 
 * @todo Funcion guarda las ip que acceden en la aplicacion
 * @author Jean Carlos Nuñez
 * @param date $today
 * @param time $hora
 * @param db $conne
 * @return mail
 *  
 */
function auditoria_ip($today,$hora,$var_ip,$conne) 
{
	phpmkr_query("insert into auditoria_ip values('$today','$hora','$var_ip') ",$conne);
	envio_coreo($var_ip,$hora,$today);
}
/**
 * 
 * @todo Funcion cambia el formato de la fecha de formato ingles a formato latino
 * @author Jean Carlos Nuñez
 * @param $value
 * @return date
 *  
 */
function fecha($value) { // fecha de YYYY/MM/DD a DD/MM/YYYY

 if ( ! empty($value) ) return substr($value,8,2) ."/". substr($value,5,2) ."/". substr($value,0,4);

}
/**
 * 
 * @todo Funcion que determina si es par o no un numero
 * @author Jean Carlos Nuñez
 * @param int $numero
 * @return boolean
 *  
 */
function esPar($numero)
{ 
   $resto = $numero%2; 
   if (($resto==0) && ($numero!=0)) 
   { 
        return true; 
   }
   else
   { 
        return false;  
   } 
}
/**
 * 
 * @todo Funcion regresa a la pagina index.php de la aplicacion
 * @author Jean Carlos Nuñez
 * @return redirect
 *  
 */
function devolver() 
{

	session_destroy();
	header("Location: index.php");
}
/**
 * 
 * @todo Funcion que convierte numero a mes
 * @author Jean Carlos Nuñez
 * @param int $mes
 * @return string
 *  
 */
function covertir_numero_mes($mes) 
{ 
	if($mes==1){echo "ENERO";}
	if($mes==2){echo "FEBRERO";}
	if($mes==3){echo "MARZO";}
	if($mes==4){echo "ABRIL";}
	if($mes==5){echo "MAYO";}
	if($mes==6){echo "JUNIO";}
	if($mes==7){echo "JULIO";}
	if($mes==8){echo "AGOSTO";}
	if($mes==9){echo "SEPTIEMBRE";}
	if($mes==10){echo "OCTUBRE";}
	if($mes==11){echo "NOVIEMBRE";}
	if($mes==12){echo "DICIEMBRE";}
}
/**
 * 
 * @todo Funcion que convierte de 12 horas a 24 horas
 * @author Jean Carlos Nuñez
 * @param time $hora
 * @return string
 *  
 */
function conversion_hora24($hora) { // Hora de 12 horas a 24 horas

	$var_len_hora = strlen($hora);
	$var_ampm = substr($hora,$var_len_hora-2,2);
	if($var_ampm=="PM")
	{
		$var_primera_hora = substr($hora,0,2);
		$var_segunda_hora = substr($hora,3,2);
		if($var_primera_hora=="12"){$hora_a_24="12:".$var_segunda_hora.":00";}					
		if($var_primera_hora=="01"){$hora_a_24="13:".$var_segunda_hora.":00";}
		if($var_primera_hora=="02"){$hora_a_24="14:".$var_segunda_hora.":00";}
		if($var_primera_hora=="03"){$hora_a_24="15:".$var_segunda_hora.":00";}		
		if($var_primera_hora=="04"){$hora_a_24="16:".$var_segunda_hora.":00";}		
		if($var_primera_hora=="05"){$hora_a_24="17:".$var_segunda_hora.":00";}		
	}
	if($var_ampm=="AM")
	{
		$var_primera_hora = substr($hora,0,2);
		$var_segunda_hora = substr($hora,3,2);
		if($var_primera_hora=="08"){$hora_a_24="08:".$var_segunda_hora.":00";}
		if($var_primera_hora=="09"){$hora_a_24="09:".$var_segunda_hora.":00";}
		if($var_primera_hora=="10"){$hora_a_24="10:".$var_segunda_hora.":00";}		
		if($var_primera_hora=="11"){$hora_a_24="11:".$var_segunda_hora.":00";}			
	}
	return $hora_a_24;
}
/**
 * 
 * @todo Funcion que convierte de 24 horas a 12 horas
 * @author Jean Carlos Nuñez
 * @param time $hora
 * @return string
 *  
 */
function conversion_hora($hora) { // Hora de 24 horas a 12 horas

$var_hora = substr($hora,0,2);
$var_minutos = substr($hora,3,2);
$var_segundos = substr($hora,6,2);


if($var_hora=='00'){if($var_hora =='00'){return "12:".$var_minutos.":".$var_segundos." AM";} exit(); }
if($var_hora <  12){return $hora." AM";}
if($var_hora =='12'){return $hora." PM";}
if($var_hora =='13'){return "01:".$var_minutos.":".$var_segundos." PM";}
if($var_hora =='14'){return "02:".$var_minutos.":".$var_segundos." PM";}
if($var_hora =='15'){return "03:".$var_minutos.":".$var_segundos." PM";}
if($var_hora =='16'){return "04:".$var_minutos.":".$var_segundos." PM";}
if($var_hora =='17'){return "05:".$var_minutos.":".$var_segundos." PM";}
if($var_hora =='18'){return "06:".$var_minutos.":".$var_segundos." PM";}
if($var_hora =='19'){return "07:".$var_minutos.":".$var_segundos." PM";}
if($var_hora =='20'){return "08:".$var_minutos.":".$var_segundos." PM";}
if($var_hora =='21'){return "09:".$var_minutos.":".$var_segundos." PM";}
if($var_hora =='22'){return "10:".$var_minutos.":".$var_segundos." PM";}
if($var_hora =='23'){return "11:".$var_minutos.":".$var_segundos." PM";}	

}
/**
 * 
 * @todo Funcion que convierte de formato en ingles a latino
 * @author Jean Carlos Nuñez
 * @param date $value
 * @return string
 *  
 */
function fecha_en($value) { // fecha de YYYY/MM/DD a DD/MM/YYYY

 if ( ! empty($value) ) return substr($value,5,2) ."/". substr($value,8,2) ."/". substr($value,0,4);

}

/**
 * 
 * @todo Funcion que convierte de fecha de 'YYYY-MM-DD HH:MM:SS' a 'DD-MM-YYYY HH:MM:SS'
 * @author Jean Carlos Nuñez
 * @param date $value
 * @return string
 *  
 */

function fechahora($value) { // fecha de 'YYYY-MM-DD HH:MM:SS' a 'DD-MM-YYYY HH:MM:SS'

 if ( ! empty($value) ) return substr($value,8,2) ."/". substr($value,5,2) ."/". substr($value,0,4)." ".substr($value,11,8);

}
/**
 * 
 * @todo Funcion que transforma la imagenes a binario para ser guardadas en la base de datos 
 * @author Jean Carlos Nuñez
 * @param string $name
 * @param string $type
 * @param string $tmp_name
 * @param int $size
 * @return binary
 *  
 */
function foto($name,$type,$tmp_name,$size)
{
		$ancho= "480"; 
		$mimetypes = array("image/jpeg", "image/pjpeg", "image/gif", "image/png");	
		if(!in_array($type, $mimetypes))
		die("El archivo que subiste no es una imagen valida");		
		$size=getimagesize($tmp_name); 
		$width=$size[0];						
		$height=$size[1]; 
		//echo $height;
		$newwidth = $ancho;
		$newheight=intval($height*$newwidth/$width); 			
		//echo "---";
		//echo $newheight;
		//exit();
		switch($type) 
		{
			case $mimetypes[0]:
			case $mimetypes[1]:
			$oldimage=imagecreatefromjpeg($tmp_name);		
			break;
			case $mimetypes[2]:
			$oldimage=imagecreatefromgif($tmp_name);		
			break;
			case $mimetypes[3]:
			$oldimage=imagecreatefrompng($tmp_name);		
			break;
		}	
		
		
		$newimage=imagecreatetruecolor($newwidth,$newheight);		
		imagecopyresampled($newimage,$oldimage,0,0,0,0,$newwidth,$newheight,$width,$height);		

		$copia="C:\$name";						
		imagejpeg($newimage,$copia);						
		//$fp = fopen($tmp_name, "rb");
		$fp = fopen($copia, "rb");
		$tfoto = fread($fp, filesize($copia));
		$tfoto = addslashes($tfoto);
		fclose($fp);
		@unlink($tmp_name);
		return $tfoto;
}

/**
 * 
 * @todo Funcion que convierte de formato en latino a inlges
 * @author Jean Carlos Nuñez
 * @param date $value
 * @return string
 *  
 */
function fecha_sql($value) { // fecha de DD/MM/YYYY a YYYYY/MM/DD

 return substr($value,6,4) ."-". substr($value,3,2) ."-". substr($value,0,2);

}
/**
 * 
 * @todo Funcion para conectarse a la base de datos
 * @author Jean Carlos Nuñez
 * @param string $HOST
 * @param string $USER
 * @param string $PASS
 * @param string $DB
 * @param string $PORT
 * @return db
 *  
 */
function phpmkr_db_connect($HOST, $USER, $PASS, $DB, $PORT)
{
	error_log("SISTEMA ACTIVO", 0);
	$conn = mysqli_connect($HOST, $USER, $PASS, $DB, $PORT) or die("No se Puede Conectar a la Base de Datos.. :-( " . mysqli_error($conn));
	return $conn;
}
/**
 * 
 * @todo Funcion para desconectar la base de datos
 * @author Jean Carlos Nuñez
 * @param db $conn
 * @return NUll
 *  
 */
function phpmkr_db_close($conn)
{
	mysqli_close($conn);
}
/**
 * 
 * @todo Funcion para hacer consultas en la aplicacion
 * @author Jean Carlos Nuñez
 * @param string $strsql
 * @param string $conn
 * @return recordset
 *  
 */
function phpmkr_query($strsql, $conn)
{
	$rs = mysqli_query($conn, $strsql);
	return $rs;
}

/**
 * 
 * @todo Funcion para contar los registros que regresa la consulta
 * @author Jean Carlos Nuñez
 * @param recordset $rs
 * @return int
 *  
 */
 
function phpmkr_num_rows($rs)
{
	return @mysqli_num_rows($rs);
}

/**
 * 
 * @todo Funcion que regresa un arreglo apartir de un recordset
 * @author Jean Carlos Nuñez
 * @param recordset $rs
 * @return array
 *  
 */
 
function phpmkr_fetch_array($rs)
{
	return mysqli_fetch_array($rs);
}
/**
 * 
 * @todo Funcion que regresa un arreglo por filas
 * @author Jean Carlos Nuñez
 * @param recordset $rs
 * @return array
 *  
 */
function phpmkr_fetch_row($rs)
{
	return mysqli_fetch_row($rs);
}
/**
 * 
 * @todo Funcion regresa un error de la base datos
 * @author Jean Carlos Nuñez
 * @param db $conn
 * @return string
 *  
 */
function phpmkr_error($conn)
{
	return mysqli_error($conn);
}


define("HOST", "localhost");
define("PORT", 3306);
define("USER", "root");
define("PASS", "142857142857");
define("DB", "portal");




/**
 * 
 * @todo Funcion construye un select a partir de una consulta
 * @author Jean Carlos Nuñez
 * @param string $field_value
 * @param string $field_label
 * @param string $sql
 * @param string $control_name
 * @param string $seleccione
 * @param db $conn
 * @return string
 *  
 */

function select($field_value, $field_label, $field_selected, $sql, $control_name, $selecione=0,$conn) 
{
      $rs = $conn->query($sql);
	  $ret="<select class='' required='true' name=".$control_name.">";
	  if ($selecione == 1) {$ret.="<option  value=''>Seleccione</option>";};
	  if ($selecione == 2) {$ret.="<option value='0'>Seleccione</option>";};
      if ($selecione == 3) {$ret.="<option value='0'>TODOS</option>";};
      while ($row_rs = $rs->fetch_assoc()) {;
        if ( $row_rs[$field_value] == $field_selected ) {;
         $ret.='<option value="'.$row_rs[$field_value].'" >'.$row_rs[$field_label].'</option>';
        } else {;
          $ret.='<option value="'.$row_rs[$field_value].'">'.$row_rs[$field_label].'</option>';
        };
      };
     $rs->close();
     $ret.="</select>";
	 print $ret;
};
/**
 * 
 * @todo Funcion construye un select a partir de una consulta
 * @author Jean Carlos Nuñez
 * @param string $field_value
 * @param string $field_label
 * @param string $sql
 * @param string $control_name
 * @param string $seleccione
 * @param db $conn
 * @return string
 *  
 */
function select2($field_value, $field_label, $field_selected, $sql, $control_name, $selecione=0,$selecione2,$selecione3,$conn) 
{
      $rs = $conn->query($sql);
	  $ret="<select class='' required='true' name=".$control_name." >";
	  if ($selecione == 1) {$ret.="<option value=''>Seleccione</option>";};
	  if ($selecione == 2) {$ret.="<option value='0'>Seleccione</option>";};
 	  if ($selecione == 3) {$ret.="<option value='0'>TODOS</option>";};
      while ($row_rs = $rs->fetch_assoc()) {;
        if ( $row_rs[$field_value] == $field_selected ) {;
         $ret.='<option value="'.$row_rs[$field_value].'" selected>'.$row_rs[$field_label].'</option>';
        } else {;
          $ret.='<option value="'.$row_rs[$field_value].'">'.$row_rs[$field_label].'</option>';
        };
      };
     $rs->close();
     $ret.="</select>";
	 print $ret;
};
/**
 * 
 * @todo Funcion construye un select a partir de una consulta
 * @author Jean Carlos Nuñez
 * @param string $field_value
 * @param string $field_label
 * @param string $sql
 * @param string $control_name
 * @param string $seleccione
 * @param db $conn
 * @return string
 *  
 */
function select3($field_value, $field_label, $field_selected, $sql, $control_name, $selecione=0,$selecione2,$selecione3,$conn) 
{
      $rs = $conn->query($sql);
	  $ret="<select class='' name=".$control_name." onchange='cambiar_valor(".cod_gru.");' >";
	  if ($selecione == 1) {$ret.="<option value=''>Seleccione</option>";};
	  if ($selecione == 2) {$ret.="<option value='0'>Seleccione</option>";};
 	  //if ($selecione == 3) {$ret.="<option value='0'>TODOS</option>";};
      while ($row_rs = $rs->fetch_assoc()) {;
        if ( $row_rs[$field_value] == $field_selected ) {;
         $ret.='<option value="'.$row_rs[$field_value].'" selected>'.$row_rs[$field_label].'</option>';
        } else {;
          $ret.='<option value="'.$row_rs[$field_value].'">'.$row_rs[$field_label].'</option>';
        };
      };
     $rs->close();
     $ret.="</select>";
	 print $ret;
};
/**
 * 
 * @todo Funcion construye un select a partir de una consulta
 * @author Jean Carlos Nuñez
 * @param string $field_value
 * @param string $field_label
 * @param string $sql
 * @param string $control_name
 * @param string $seleccione
 * @param db $conn
 * @return string
 *  
 */
function select4($field_value, $field_label, $field_selected, $sql, $control_name, $selecione=0,$selecione2,$selecione3,$conn) 
{
      $rs = $conn->query($sql);
	  $ret="<select class='' name=".$control_name." >";
	  if ($selecione == 1) {$ret.="<option value=''>Unidad Sugerida</option>";};
	  if ($selecione == 2) {$ret.="<option value='9000'>Unidad Sugerida</option>";};
 	  //if ($selecione == 3) {$ret.="<option value='0'>TODOS</option>";};
      while ($row_rs = $rs->fetch_assoc()) {;
        if ( $row_rs[$field_value] == $field_selected ) {;
         $ret.='<option value="'.$row_rs[$field_value].'" selected>'.$row_rs[$field_label].'</option>';
        } else {;
          $ret.='<option value="'.$row_rs[$field_value].'">'.$row_rs[$field_label].'</option>';
        };
      };
     $rs->close();
     $ret.="</select>";
	 print $ret;
};
/**
 * 
 * @todo Funcion construye un select a partir de una consulta
 * @author Jean Carlos Nuñez
 * @param string $field_value
 * @param string $field_label
 * @param string $sql
 * @param string $control_name
 * @param string $seleccione
 * @param db $conn
 * @return string
 *  
 */
function select5($field_value, $field_label, $field_selected, $sql, $control_name, $selecione=0,$conn) 
{
      $rs = $conn->query($sql);
	  $ret="<select class='' onClick='buscar_diario();' name=".$control_name.">";
	  if ($selecione == 1) {$ret.="<option  value=''>Seleccione</option>";};
	  if ($selecione == 2) {$ret.="<option value='0'>Seleccione</option>";};
      if ($selecione == 3) {$ret.="<option value='0'>TODOS</option>";};
      while ($row_rs = $rs->fetch_assoc()) {;
        if ( $row_rs[$field_value] == $field_selected ) {;
         $ret.='<option value="'.$row_rs[$field_value].'" >'.$row_rs[$field_label].'</option>';
        } else {;
          $ret.='<option value="'.$row_rs[$field_value].'">'.$row_rs[$field_label].'</option>';
        };
      };
     $rs->close();
     $ret.="</select>";
	 print $ret;
};
/**
 * 
 * @todo Funcion construye un select a partir de una consulta
 * @author Jean Carlos Nuñez
 * @param string $field_value
 * @param string $field_label
 * @param string $sql
 * @param string $control_name
 * @param string $seleccione
 * @param db $conn
 * @return string
 *  
 */
function select6($field_value, $field_label, $field_selected, $sql, $control_name, $selecione=0,$selecione2,$selecione3,$conn) 
{
      $rs = $conn->query($sql);
	  $ret="<select class='' onClick='buscar_diario();' name=".$control_name." >";
	  if ($selecione == 1) {$ret.="<option value=''>Seleccione</option>";};
	  if ($selecione == 2) {$ret.="<option value='0'>Seleccione</option>";};
 	  //if ($selecione == 3) {$ret.="<option value='0'>TODOS</option>";};
      while ($row_rs = $rs->fetch_assoc()) {;
        if ( $row_rs[$field_value] == $field_selected ) {;
         $ret.='<option value="'.$row_rs[$field_value].'" selected>'.$row_rs[$field_label].'</option>';
        } else {;
          $ret.='<option value="'.$row_rs[$field_value].'">'.$row_rs[$field_label].'</option>';
        };
      };
     $rs->close();
     $ret.="</select>";
	 print $ret;
};
/**
 * 
 * @todo Funcion construye un select a partir de una consulta
 * @author Jean Carlos Nuñez
 * @param string $field_value
 * @param string $field_label
 * @param string $sql
 * @param string $control_name
 * @param string $seleccione
 * @param db $conn
 * @return string
 *  
 */
function select7($field_value, $field_label, $field_selected, $sql, $control_name, $selecione=0,$conn) 
{
      $rs = $conn->query($sql);
	  $ret="<select class='' name=".$control_name." onclick='buscar_auto_diarios();'>";
	  if ($selecione == 1) {$ret.="<option  value=''>Seleccione</option>";};
	  if ($selecione == 2) {$ret.="<option value='0'>Seleccione</option>";};
      if ($selecione == 3) {$ret.="<option value='0'>TODOS</option>";};
      while ($row_rs = $rs->fetch_assoc()) {;
        if ( $row_rs[$field_value] == $field_selected ) {;
         $ret.='<option value="'.$row_rs[$field_value].'" >'.$row_rs[$field_label].'</option>';
        } else {;
          $ret.='<option value="'.$row_rs[$field_value].'">'.$row_rs[$field_label].'</option>';
        };
      };
     $rs->close();
     $ret.="</select>";
	 print $ret;
};
/**
 * 
 * @todo Funcion construye un select a partir de una consulta
 * @author Jean Carlos Nuñez
 * @param string $field_value
 * @param string $field_label
 * @param string $sql
 * @param string $control_name
 * @param string $seleccione
 * @param db $conn
 * @return string
 *  
 */
function select8($field_value, $field_label, $field_selected, $sql, $control_name, $selecione=0,$conn) 
{
      $rs = $conn->query($sql);
	  $ret="<select class='' required='true' name=".$control_name." onclick='buscar_ope();'>";
	  if ($selecione == 1) {$ret.="<option  value=''>Seleccione</option>";};
	  if ($selecione == 2) {$ret.="<option value='0'>Seleccione</option>";};
      if ($selecione == 3) {$ret.="<option value='0'>TODOS</option>";};
      while ($row_rs = $rs->fetch_assoc()) {;
        if ( $row_rs[$field_value] == $field_selected ) {;
         $ret.='<option value="'.$row_rs[$field_value].'" >'.$row_rs[$field_label].'</option>';
        } else {;
          $ret.='<option value="'.$row_rs[$field_value].'">'.$row_rs[$field_label].'</option>';
        };
      };
     $rs->close();
     $ret.="</select>";
	 print $ret;
};
/**
 * 
 * @todo Funcion construye un select a partir de una consulta
 * @author Jean Carlos Nuñez
 * @param string $field_value
 * @param string $field_label
 * @param string $sql
 * @param string $control_name
 * @param string $seleccione
 * @param db $conn
 * @return string
 *  
 */
function select10($field_value, $field_label, $field_selected, $sql, $control_name, $selecione=0,$selecione2,$selecione3,$conn,$onchange) 
{
      $rs = $conn->query($sql);
	  $ret="<select class='' required='true'  onchange='".$onchange."' name=".$control_name." >";
	  if ($selecione == 1) {$ret.="<option value=''>Seleccione</option>";};
	  if ($selecione == 2) {$ret.="<option value='0'>Seleccione</option>";};
 	  if ($selecione == 3) {$ret.="<option value='0'>TODOS</option>";};
      while ($row_rs = $rs->fetch_assoc()) {;
        if ( $row_rs[$field_value] == $field_selected ) {;
         $ret.='<option value="'.$row_rs[$field_value].'" selected>'.utf8_encode($row_rs[$field_label]).'</option>';
        } else {;
          $ret.='<option value="'.$row_rs[$field_value].'">'.utf8_encode($row_rs[$field_label]).'</option>';
        };
      };
     $rs->close();
     $ret.="</select>";
	 print $ret;
};
/**
 * 
 * @todo Funcion construye un select a partir de una consulta
 * @author Jean Carlos Nuñez
 * @param string $field_value
 * @param string $field_label
 * @param string $sql
 * @param string $control_name
 * @param string $seleccione
 * @param db $conn
 * @return string
 *  
 */
function select9($field_value, $field_label, $field_selected, $sql, $control_name, $selecione=0,$selecione2,$selecione3,$conn,$onchange) 
{
      $rs = $conn->query($sql);
	  $ret="<select class='' required='true' onchange='".$onchange."' name=".$control_name." >";
	  if ($selecione == 1) {$ret.="<option value=''>Seleccione</option>";};
	  if ($selecione == 2) {$ret.="<option value='0'>Seleccione</option>";};
 	  if ($selecione == 3) {$ret.="<option value='0'>TODOS</option>";};
      while ($row_rs = $rs->fetch_assoc()) {;
        if ( $row_rs[$field_value] == $field_selected ) {;
         $ret.='<option value="'.$row_rs[$field_value].'" selected>'.$row_rs[$field_label].'</option>';
        } else {;
          $ret.='<option value="'.$row_rs[$field_value].'">'.$row_rs[$field_label].'</option>';
        };
      };
     $rs->close();
     $ret.="</select>";
	 print $ret;
};
/**
 * 
 * @todo Funcion construye un select a partir de una consulta
 * @author Jean Carlos Nuñez
 * @param string $field_value
 * @param string $field_label
 * @param string $sql
 * @param string $control_name
 * @param string $seleccione
 * @param db $conn
 * @return string
 *  
 */
function select11($field_value, $field_label, $field_selected, $sql, $control_name, $selecione=0,$selecione2,$selecione3,$conn,$onchange) 
{
      $rs = $conn->query($sql);
	  $ret="<select class='' required='true' readonly='true' onchange='".$onchange."' name=".$control_name." >";	  
      while ($row_rs = $rs->fetch_assoc()) {;
        if ( $row_rs[$field_value] == $field_selected ) {;
         $ret.='<option value="'.$row_rs[$field_value].'" selected>'.$row_rs[$field_label].'</option>';
        } else {;
          $ret.='<option value="'.$row_rs[$field_value].'">'.$row_rs[$field_label].'</option>';
        };
      };
     $rs->close();
     $ret.="</select>";
	 print $ret;
};
/**
 * 
 * @todo Funcion construye un select a partir de una consulta
 * @author Jean Carlos Nuñez
 * @param string $field_value
 * @param string $field_label
 * @param string $sql
 * @param string $control_name
 * @param string $seleccione
 * @param db $conn
 * @return string
 *  
 */
function select12($field_value, $field_label, $field_selected, $sql, $control_name, $selecione=0,$selecione2,$selecione3,$conn) 
{ 
      $rs = $conn->query($sql);
	  $ret="<select class='' required='true' name=".$control_name." >";
	  if ($selecione == 1) {$ret.="<option value=''>Seleccione</option>";};
	  if ($selecione == 2) {$ret.="<option value='0'>Seleccione</option>";};
 	  if ($selecione == 3) {$ret.="<option value='0'>TODOS</option>";};
      while ($row_rs = $rs->fetch_assoc()) {;
        if ( "'".$row_rs[$field_value]."'" == "'".$field_selected."'" ) {;
         $ret.='<option value="'.$row_rs[$field_value].'" selected>'.$row_rs[$field_label].'</option>';
        } else {;
          $ret.='<option value="'.$row_rs[$field_value].'">'.$row_rs[$field_label].'</option>';
        };      
      };
     $rs->close(); 
     $ret.="</select>";
	 print $ret;
};
/**
 * 
 * @todo Funcion construye un select a partir de una consulta
 * @author Jean Carlos Nuñez
 * @param string $field_value
 * @param string $field_label
 * @param string $sql
 * @param string $control_name
 * @param string $seleccione
 * @param db $conn
 * @return string
 *  
 */
function select13($field_value, $field_label, $field_selected, $sql, $control_name, $selecione=0,$selecione2,$selecione3,$conn,$onchange) 
{
      $rs = $conn->query($sql);
	  $ret="<select title='Porcentaje de Costo' class='' required='true' onchange='".$onchange."' name=".$control_name." >";
	  if ($selecione == 1) {$ret.="<option value=''>Seleccione</option>";};
	  if ($selecione == 2) {$ret.="<option value='0'>Seleccione</option>";};
 	  if ($selecione == 3) {$ret.="<option value='0'>TODOS</option>";};
      while ($row_rs = $rs->fetch_assoc()) {;
        if ( $row_rs[$field_value] == $field_selected ) {;
         $ret.='<option value="'.$row_rs[$field_value].'" selected>'.$row_rs[$field_label].'</option>';
        } else {;
          $ret.='<option value="'.$row_rs[$field_value].'">'.$row_rs[$field_label].'</option>';
        };
      };
     $rs->close();
     $ret.="</select>";
	 print $ret;
};

/**
 * 
 * @todo Funcion construye un select a partir de una consulta
 * @author Jean Carlos Nuñez
 * @param string $field_value
 * @param string $field_label
 * @param string $sql
 * @param string $control_name
 * @param string $seleccione
 * @param db $conn
 * @return string
 *  
 */

function select14($field_value, $field_label, $field_selected, $sql, $control_name, $selecione=0,$conn,$onchange) 
{
      $rs = $conn->query($sql);
	  $ret="<select class='' required='true' onchange='$onchange' name=".$control_name.">";
	  if ($selecione == 1) {$ret.="<option  value=''>Seleccione</option>";};
	  if ($selecione == 2) {$ret.="<option value='0'>Seleccione</option>";};
      if ($selecione == 3) {$ret.="<option value='0'>TODOS</option>";};
      while ($row_rs = $rs->fetch_assoc()) {;
        if ( $row_rs[$field_value] == $field_selected ) {;
         $ret.='<option value="'.$row_rs[$field_value].'" selected>'.$row_rs[$field_label].'</option>';
        } else {;
          $ret.='<option value="'.$row_rs[$field_value].'">'.$row_rs[$field_label].'</option>';
        };
      };
     $rs->close();
     $ret.="</select>";
	 print $ret;
};


/**
 * 
 * @todo Funcion construye un select a partir de una consulta
 * @author Jean Carlos Nuñez
 * @param string $field_value
 * @param string $field_label
 * @param string $sql
 * @param string $control_name
 * @param string $seleccione
 * @param db $conn
 * @return string
 *  
 */
function select15($field_value, $field_label, $field_selected, $sql, $control_name, $selecione=0,$selecione2,$selecione3,$conn) 
{
      $rs = $conn->query($sql);
	  $ret="<select class=''  name=".$control_name." >";
	  if ($selecione == 1) {$ret.="<option value=''>Seleccione</option>";};
	  if ($selecione == 2) {$ret.="<option value='0'>Seleccione</option>";};
 	  if ($selecione == 3) {$ret.="<option value='0'>TODOS</option>";};
      while ($row_rs = $rs->fetch_assoc()) {;
        if ( $row_rs[$field_value] == $field_selected ) {;
         $ret.='<option value="'.$row_rs[$field_value].'" selected>'.$row_rs[$field_label].'</option>';
        } else {;
          $ret.='<option value="'.$row_rs[$field_value].'">'.$row_rs[$field_label].'</option>';
        };
      };
     $rs->close();
     $ret.="</select>";
	 print $ret;
};

/**
 * 
 * @todo Clase que convierte de numero a letras
 * @author Jean Carlos Nuñez
 * @return string
 *  
 */
class EnLetras
{
  var $Void = "";
  var $SP = " ";
  var $Dot = ".";
  var $Zero = "0";
  var $Neg = "Menos";
  
function ValorEnLetras($x, $Moneda ) 
{
    $s="";
    $Ent="";
    $Frc="";
    $Signo="";
        
    if(floatVal($x) < 0)
     $Signo = $this->Neg . " ";
    else
     $Signo = "";
    
    if(intval(number_format($x,2,'.','') )!=$x) //<- averiguar si tiene decimales
      $s = number_format($x,2,'.','');
    else
      $s = number_format($x,0,'.','');
       
    $Pto = strpos($s, $this->Dot);
        
    if ($Pto === false)
    {
      $Ent = $s;
      $Frc = $this->Void;
    }
    else
    {
      $Ent = substr($s, 0, $Pto );
      $Frc =  substr($s, $Pto+1);
    }

    if($Ent == $this->Zero || $Ent == $this->Void)
       $s = "Cero ";
    elseif( strlen($Ent) > 7)
    {
       $s = $this->SubValLetra(intval( substr($Ent, 0,  strlen($Ent) - 6))) . 
             "Millones " . $this->SubValLetra(intval(substr($Ent,-6, 6)));
    }
    else
    {
      $s = $this->SubValLetra(intval($Ent));
    }

    if (substr($s,-9, 9) == "Millones " || substr($s,-7, 7) == "Mill贸n ")
       $s = $s . "de ";

    $s = $s . $Moneda;

    if($Frc != $this->Void)
    {
       $s = $s . " Con " . $this->SubValLetra(intval($Frc)) . "Centavos";
       //$s = $s . " " . $Frc . "/100";
    }
    return ($Signo . $s . " ");
   
}


function SubValLetra($numero) 
{
    $Ptr="";
    $n=0;
    $i=0;
    $x ="";
    $Rtn ="";
    $Tem ="";

    $x = trim("$numero");
    $n = strlen($x);

    $Tem = $this->Void;
    $i = $n;
    
    while( $i > 0)
    {
       $Tem = $this->Parte(intval(substr($x, $n - $i, 1). 
                           str_repeat($this->Zero, $i - 1 )));
       If( $Tem != "Cero" )
          $Rtn .= $Tem . $this->SP;
       $i = $i - 1;
    }

    
    //--------------------- GoSub FiltroMil ------------------------------
    $Rtn=str_replace(" Mil Mil", " Un Mil", $Rtn );
    while(1)
    {
       $Ptr = strpos($Rtn, "Mil ");       
       If(!($Ptr===false))
       {
          If(! (strpos($Rtn, "Mil ",$Ptr + 1) === false ))
            $this->ReplaceStringFrom($Rtn, "Mil ", "", $Ptr);
          Else
           break;
       }
       else break;
    }

    //--------------------- GoSub FiltroCiento ------------------------------
    $Ptr = -1;
    do{
       $Ptr = strpos($Rtn, "Cien ", $Ptr+1);
       if(!($Ptr===false))
       {
          $Tem = substr($Rtn, $Ptr + 5 ,1);
          if( $Tem == "M" || $Tem == $this->Void)
             ;
          else          
             $this->ReplaceStringFrom($Rtn, "Cien", "Ciento", $Ptr);
       }
    }while(!($Ptr === false));

    //--------------------- FiltroEspeciales ------------------------------
    $Rtn=str_replace("Diez Un", "Once", $Rtn );
    $Rtn=str_replace("Diez Dos", "Doce", $Rtn );
    $Rtn=str_replace("Diez Tres", "Trece", $Rtn );
    $Rtn=str_replace("Diez Cuatro", "Catorce", $Rtn );
    $Rtn=str_replace("Diez Cinco", "Quince", $Rtn );
    $Rtn=str_replace("Diez Seis", "Dieciseis", $Rtn );
    $Rtn=str_replace("Diez Siete", "Diecisiete", $Rtn );
    $Rtn=str_replace("Diez Ocho", "Dieciocho", $Rtn );
    $Rtn=str_replace("Diez Nueve", "Diecinueve", $Rtn );
    $Rtn=str_replace("Veinte Un", "Veintiun", $Rtn );
    $Rtn=str_replace("Veinte Dos", "Veintidos", $Rtn );
    $Rtn=str_replace("Veinte Tres", "Veintitres", $Rtn );
    $Rtn=str_replace("Veinte Cuatro", "Veinticuatro", $Rtn );
    $Rtn=str_replace("Veinte Cinco", "Veinticinco", $Rtn );
    $Rtn=str_replace("Veinte Seis", "Veintise铆s", $Rtn );
    $Rtn=str_replace("Veinte Siete", "Veintisiete", $Rtn );
    $Rtn=str_replace("Veinte Ocho", "Veintiocho", $Rtn );
    $Rtn=str_replace("Veinte Nueve", "Veintinueve", $Rtn );

    //--------------------- FiltroUn ------------------------------
    If(substr($Rtn,0,1) == "M") $Rtn = "Un " . $Rtn;
    //--------------------- Adicionar Y ------------------------------
    for($i=65; $i<=88; $i++)
    {
      If($i != 77)
         $Rtn=str_replace("a " . Chr($i), "* y " . Chr($i), $Rtn);
    }
    $Rtn=str_replace("*", "a" , $Rtn);
    return($Rtn);
}


function ReplaceStringFrom(&$x, $OldWrd, $NewWrd, $Ptr)
{
  $x = substr($x, 0, $Ptr)  . $NewWrd . substr($x, strlen($OldWrd) + $Ptr);
}


function Parte($x)
{
    $Rtn='';
    $t='';
    $i='';
    Do
    {
      switch($x)
      {
         Case 0:  $t = "Cero";break;
         Case 1:  $t = "Un";break;
         Case 2:  $t = "Dos";break;
         Case 3:  $t = "Tres";break;
         Case 4:  $t = "Cuatro";break;
         Case 5:  $t = "Cinco";break;
         Case 6:  $t = "Seis";break;
         Case 7:  $t = "Siete";break;
         Case 8:  $t = "Ocho";break;
         Case 9:  $t = "Nueve";break;
         Case 10: $t = "Diez";break;
         Case 20: $t = "Veinte";break;
         Case 30: $t = "Treinta";break;
         Case 40: $t = "Cuarenta";break;
         Case 50: $t = "Cincuenta";break;
         Case 60: $t = "Sesenta";break;
         Case 70: $t = "Setenta";break;
         Case 80: $t = "Ochenta";break;
         Case 90: $t = "Noventa";break;
         Case 100: $t = "Cien";break;
         Case 200: $t = "Doscientos";break;
         Case 300: $t = "Trescientos";break;
         Case 400: $t = "Cuatrocientos";break;
         Case 500: $t = "Quinientos";break;
         Case 600: $t = "Seiscientos";break;
         Case 700: $t = "Setecientos";break;
         Case 800: $t = "Ochocientos";break;
         Case 900: $t = "Novecientos";break;
         Case 1000: $t = "Mil";break;
         Case 1000000: $t = "Mill贸n";break;
      }

      If($t == $this->Void)
      {
        $i = $i + 1;
        $x = $x / 1000;
        If($x== 0) $i = 0;
      }
      else
         break;
           
    }while($i != 0);
   
    $Rtn = $t;
    Switch($i)
    {
       Case 0: $t = $this->Void;break;
       Case 1: $t = " Mil";break;
       Case 2: $t = " Millones";break;
       Case 3: $t = " Billones";break;
    }
    return($Rtn . $t);
}

}
?>